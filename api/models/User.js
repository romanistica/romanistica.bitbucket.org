/* global EnumService */
/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcryptjs');

module.exports = {
  schema: true,

  attributes: {
    name: {
      type: 'string',
      required: true,
    },
    
    username: {
      type: 'string',
      unique: true,
      required: true,
    },
    
    motto: {
      type: 'string',
    },

    level: {
      type: 'string',
			enum: EnumService.userLevels.singulars,
    },

    status: {
      type: 'string',
    },
    
    scientific: {
      type: 'string',
    },

    didactic: {
      type: 'string',
    },

    administrative: {
      type: 'string',
    },

    photo: {
      type: 'string',
    },

    department: {
      model: 'Department',
    },

    contact: {
      type: 'string',
    },

    email: {
      type: 'email',
      required: true,
      unique: true,
    },
    
    admin: {
      type: 'boolean',
      defaultsTo: false,
    },
    
    hidden: {
      type: 'boolean',
      defaultsTo: false,
    },
    
    encryptedPassword: {
      type: 'string'
      // Why not require the password?
    },
    
    events: {
      collection: 'Event',
      via: 'users',
      dominant: true,
    },
    
    projects: {
      collection: 'Project',
      via: 'users',
      dominant: true,
    },

    journals: {
      collection: 'Journal',
      via: 'users',
      dominant: true,
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      delete obj.confirmation;
      delete obj.encryptedPassword;
      delete obj._csrf;
      return obj;
    }
  },
  
  beforeCreate: function(values, next) {
    if (!values.password || values.password != values.confirmation) {
      return next({ err: ["Password doesnt match with confirmation."] });
    }
    
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(values.password, salt, function(err, encryptedPassword) {
        if (err) {
          return next(err);
        }
      
        values.encryptedPassword = encryptedPassword;
        next(null, values);
      });
    });
  },

  beforeUpdate: function(values, next) {
    if (!values.updatedPassword) {
      return next(null, values);
    }
    
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(values.updatedPassword, salt, function(err, encryptedPassword) {
        if (err) {
          return next(err);
        }
      
        values.encryptedPassword = encryptedPassword;
        next(null, values);
      });
    });
  },
};
