module.exports = function(req, res, ok) {
	var noRightsError = [{
		name: 'noRights',
		message: 'You must be authenticated.'
	}];
	
	if (!req.session || !req.session.User) {
		req.session.flash = {
			err: noRightsError
		};
		
		return res.redirect('/');
	}
	
	var sessionUserMatchesId = req.session.User.id === req.param('id') || req.session.User.username === req.param('id');
	var isAdmin = req.session.User.admin;
	
	if (!(sessionUserMatchesId || isAdmin)) {
		req.session.flash = {
			err: noRightsError
		};
		
		return res.redirect('/');
	}
	
	return ok();
};