var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

ProjectWidget = isNode ? require('../widgets/entities/project-widget.jsx').ProjectWidget : window.ProjectWidget;

var ProjectsPage = React.createClass({
	propTypes: {
		projects: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
	},

	render: function() {
		var self = this;
	
		return (
			<div className="projects-page table">
				<div className="row">
					<div className="col">
						<div className="container">
							<h1>Proiecte</h1>
							<div className="projects">
								{self.props.projects.map(function(project) {
									return (
										<ProjectWidget project={project} />
									);
								})}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

if (isNode) {
	exports.ProjectsPage = ProjectsPage;
}