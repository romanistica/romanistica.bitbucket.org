var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var EventPage = React.createClass({
	propTypes: {
		event: React.PropTypes.object,
	},

	render: function() {
		var self = this;
		
		if (!self.props.event) {
			return (<div className="event-widget" />);
		}

		return (
			<div className="event-page">
				<img src="images/profs/alexandru.gafton.jpg" alt="Alexandru Gafton" />
				<h1>Evenimentul Universitaria</h1>
				<h3>Locaţia</h3>
				<h4>Perioada</h4>
				<h5>Persoane</h5>
				<nav>
				</nav>
			</div>
		);
	}
});

if (isNode) {
	exports.EventPage = EventPage;
}