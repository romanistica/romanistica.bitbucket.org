var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var ProjectPage = React.createClass({
	propTypes: {
		project: React.PropTypes.object,
	},

	render: function() {
		var self = this;

		if (!self.props.user) {
			return (<div className="project-widget" />);
		}

		return (
			<div className="project-page">
				<img src="images/profs/alexandru.gafton.jpg" alt="Alexandru Gafton" />
				<h1>Proiectul Universitaria</h1>
				<h2>Locaţia</h2>
				<h3>Perioada</h3>
				<article>
					<h4>Didactic</h4>
					<section>Lorem Ipsum</section>
				</article>
				<article>
					<h4>Persoane</h4>
					<nav>
						<a href="">Ion Popescu</a>
					</nav>
				</article>
			</div>
		);
	}
});

if (isNode) {
	exports.ProjectPage = ProjectPage;
}