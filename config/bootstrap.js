/* global Project */
/* global Department */
/* global Program */
/* global Brand */
/* global User */
/* global Reward */
/* global Question */
/* global QuestionAnswer */
/* global QuestionGroup */
/* global Drive */

/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var async = require('async');

var insertDepartments = function(next) {
	Department.count().exec(function(err, count) {
		if (err) {
			return next(err);
		}
		
		var departments = [
			{
				name: 'Catedra de Limbă română pentru studenți străini',
				users: [
					{
						// Zaharia Ciaușu
						name: 'Zaharia Ciaușu',
						username: 'zaharia-ciausu',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/zaharia-ciausu.jpg',
						email: 'zaharia-ceausu@romanistica.uaic.ro',
						password: 'tDc2WFlcEC6XH1sIr6fi',
						confirmation: 'tDc2WFlcEC6XH1sIr6fi',
						admin: false,
						hidden: false,
					},
					{
						// Ioana Moldovanu-Cenușă
						name: 'Ioana Moldovanu-Cenușă',
						username: 'ioana-moldovanu-cenusa',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'ioana-moldovanu-cenusa@romanistica.uaic.ro',
						password: 'eOOvlya7xlc1eGgbvZvr',
						confirmation: 'eOOvlya7xlc1eGgbvZvr',
						admin: false,
						hidden: false,
					},
					{
						// Carmen Livia Tudor
						name: 'Carmen Livia Tudor',
						username: 'carmen-livia-tudor',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/carmen-livia-tudor.jpg',
						email: 'carmen-livia-tudor@romanistica.uaic.ro',
						password: 'jkKtHPwcrzT7ZGHgpbwh',
						confirmation: 'jkKtHPwcrzT7ZGHgpbwh',
						admin: false,
						hidden: false,
					},
					{
						// Gina Nimigean
						name: 'Gina Nimigean',
						username: 'gina-nimigean',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/gina-nimigean.jpg',
						email: 'gina-nimigean@romanistica.uaic.ro',
						password: '4fjUr5IGAPoh66nUtp7z',
						confirmation: '4fjUr5IGAPoh66nUtp7z',
						admin: false,
						hidden: false,
					},
					{
						// Vladina Munteanu
						name: 'Vladina Munteanu',
						username: 'vladina-munteanu',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'vladina-munteanu@romanistica.uaic.ro',
						password: 'FBOVJb5hjgm7M9TuVAZV',
						confirmation: 'FBOVJb5hjgm7M9TuVAZV',
						admin: false,
						hidden: false,
					},
					{
						// Iolanda Sterpu
						name: 'Iolanda Sterpu',
						username: 'iolanda-sterpu',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/iolanda-sterpu.jpg',
						email: 'iolanda-sterpu@romanistica.uaic.ro',
						password: 'teMYEP5N7tl8UJvSjxwC',
						confirmation: 'teMYEP5N7tl8UJvSjxwC',
						admin: false,
						hidden: false,
					},
					{
						// Ludmila Braniște
						name: 'Ludmila Braniște',
						username: 'ludmila-braniste',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/ludmila-braniste.jpg',
						email: 'ludmila-braniste@romanistica.uaic.ro',
						password: 'cR7WD0BN2ovowf3d4DOZ',
						confirmation: 'cR7WD0BN2ovowf3d4DOZ',
						admin: false,
						hidden: false,
					},
				],
			},
			{
				name: 'Departamentul tehnic',
				hidden: true,
				users: [
					{
						// Vlad Manea (admin)
						name: 'Vlad Manea',
						username: 'vlad-manea',
						email: 'vlad.c.manea@gmail.com',
						password: 'Romanistica4Nexus',
						confirmation: 'Romanistica4Nexus',
						admin: true,
						hidden: true,
					},
				],
			},
			{					
				name: 'Catedra de Limba română și Lingvistică generală',
				users: [
					{
						// Ioan Milică (admin)
						name: 'Ioan Milică',
						username: 'ioan-milica',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nCoordonator al colecției [„Logos”](http://www.editura.uaic.ro/colectii.php?colectie=logos), Editura Universității „Alexandru Ioan Cuza”\n\nEditor al anuarului [„Text și discurs religios”](http://www.cntdr.ro/revista)\n\nMembru (secretar de redacție) în colegiul editorial al revistei „Analele Științifice ale Universității „Alexandru Ioan Cuza” din Iași, secţiunea III e, Lingvistică\n\nMembru în colegiul științific al revistei [„Limba română”](http://limbaromana.md), Chișinău\n\nMembru în comitetul științific al revistei [„Argotica. Revistă internațională de studii argotice”](http://cis01.central.ucv.ro/litere/argotica/1.%20Argotica_Ro/3.%20Comitet%20stiintific.htm)',
						scientific: '# Volume\n\n* **Expresivitatea argoului**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2009\n\n* **Lumi discursive**, Editura Junimea, Iași, 2013\n\n* **Noțiuni de stilistică**, Editura Vasiliana ‘98, 2014\n\n# Ediții\n\n* Dumitru Irimia, **Curs de lingvistică generală**, postfață de Ilie Moisuc, ediţia a III-a, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2011\n\n* Dumitru Irimia, **Studii eminesciene**, Editura Universității „Alexandru Ioan Cuza” din Iaşi, 2014 (co-editor: Ilie Moisuc)\n\n# Colaborare la lucrări de referință\n\n* Marinescu, Cornelia (coord.), **Enciclopedia Concisă Britannica**, București, Editura Litera, 2009\n\n* Câmpeanu, Ilieș, Marinescu, Cornelia (coord.), **Enciclopedia Universală Britannica**, 16 volume, București, Editura Litera, 2010\n\n# Volume ale unor manifestări științifice\n\n* **Perspective asupra textului și discursului religios**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2013 (co-editori: Emanuel Gafton, Sorin Guia)\n\n* **Limba română azi**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2007 (editor coordonator: Dumitru Irimia, co-editor: Ana-Maria Minuț)\n\n# Articole (selectiv)\n\n* **Theatrum mundi în publicistica lui Mihai Eminescu**, în Studii eminescologice, Editura Clusium, Cluj-Napoca, nr. 16/2014, p. 123-144.\n\n* **Proverbes et anti-proverbes**, Philologica Jassyensia, ISSN 1841-3347, Editura Alfa, Iași, anul IX, nr. 1/2013, p. 63-79.\n\n* **Eminescu despre limbă și literatură**, Limba română, Chișinău, Republica Moldova,, publicație editată cu sprijinul Ministerului Afacerilor Externe al României – Departamentul Politici pentru Relația cu Românii de Pretutindeni, an XXIII, nr. 7-8, p. 40-52.\n\n* **Mozaic și basorelief în publicistica lui Mihai Eminescu**, Studii eminescologice, Editura Clusium, Cluj-Napoca, nr. 15/2013, p. 9-35.\n\n* **Plant Names: A Cognitive Approach**, în Vladimir Polyakov, Valery Solovyev (eds.), Cognitive Modeling in Linguistics, Cambridge Scholars Publishing, Newcastle upon Tyne, p. 105-120.\n\n* **Teoria cadrelor semantice: aspecte teoretice, implicații metodologice și potențial de aplicare**, Analele Universității de Vest din Timișoara, seria: Științe Filologice, Editura Universității de Vest, Timișoara, vol. L, 2012, p. 103-114.\n\n* **Norma argotică**, Argotica, nr. 1, 2012, p. 134-154.\n\n* **Dublete sinonimice în discursul religios**, Analele Ştiinţifice ale Universităţii „Alexandru Ioan Cuza”, secţiunea III e, Lingvistică, tom LVIII, 2012, In memoriam Vasile Arvinte, p. 155-178 (co-autor: Alexandru Gafton).\n\n* **Notes on the Semantics of the Romanian cruce: from Lexis to Proverbs**, Text și discurs religios, Editura Universității „Alexandru Ioan Cuza”, Iași, nr. 4, 2012, p. 319-331.\n\n* **Basarabia, orizont eminescian**, Limba română, Chișinău, Republica Moldova, publicație editată cu sprijinul Institutului Cultural Român, an XXII, nr. 5-6, p. 29-41.\n\n* **Stilistica, în concepția lui E. Coșeriu. Note de lectură**, Limba română, Chișinău, Republica Moldova, publicație editată cu sprijinul Institutului Cultural Român, an XXII, nr. 11-12, p. 17-24.\n\n* **Violenţa de limbaj în discursul politic actual. Studiu de caz privind discursul parlamentar românesc**, Sfera Politicii, volum XIX, nr. 10 (164), octombrie 2011, Fundaţia Societatea Civilă, Bucureşti, ISSN 121-6720, p. 32-42.\n\n* **Christian Imagery in Romanian Folk Plant Names**, Text și discurs religios, Editura Universității „Alexandru Ioan Cuza”, Iași, nr. 5/2013, p. 313-334.\n\n* **Făt-Frumos din lacrimă: arta portretizării**, în Gheorghe Chivu, Alexandru Gafton, Adina Chirilă (coord.), Filologie şi Bibliologie, Editura Universităţii de Vest, Timişoara, 2011, p. 213-231.\n\n* **Argoul în discursul parlamentar**, în Rodica Zafiu, Adina Dragomirescu, Alexandru Nicolae (editori), Limba română. Controverse, delimitări, noi ipoteze, Editura Universităţii din Bucureşti, 2010, p. 155-165.\n\n* **Naive and Expert Models in the Linguistic Representation of Reality: the Names of Plants**, Language and Literature. European Landmarks of Identity, nr. 6, 2010, Editura Universităţii din Piteşti, p. 70-78.\n\n* **L’agression des noms dans la communication par le biais de l’Internet**, Studii şi Cercetări de Onomastică şi Lexicologie, Editura Sitech, Craiova, an III, nr. 1-2, 2010, p. 168-179.\n\n* **Grammaticalisation et expressivité**, Revue Roumaine de Linguistique, Editura Academiei Române, Bucureşti, nr. 3 (LV), 2010, p. 301-312.\n\n* **Poezie şi limbaj**, Caietele de la Putna, Editura Nicodim Califragul, Mănăstirea Putna, nr. 3, 2010, p. 139-151.\n\n# Comunicări științifice (selectiv)\n\n* **Relațiile dialectologiei cu stilistica. Câteva aspecte teoretice**, Cel de-al XVI-lea Simpozion Internațional de Dialectologie, Academia Română, Institutul de Lingvistică și Istorie Literară „Sextil Pușcariu”, Cluj-Napoca, 11-12 septembrie 2014 (co-autor: Sorin Guia)\n\n* **Imaginarul gastronomic al lui Păstorel”, Simpozionul Național „Iașii Teodorenilor**, Festivalul Internațional al Educației, Biblioteca Județeană „Gheorghe Asachi”, Iași, 26 iunie 2014\n\n* **Reprezentări ale mecanicității în publicistica eminesciană**, Simpozionul Internațional „Eminescu: Carte-Cultură-Civilizație”, Biblioteca Județeană „Mihai Eminescu”, Botoșani, 14 iunie 2014.\n\n* **Fitonimele și toponimele. Studiu asupra materialului documentar publicat în Documenta Romaniae Historica (DRH)**, Simpozionul naţional Toponimia între istorie, geografie şi lingvistică, organizatori: Institutul de Filologie Română „A. Philippide” și Asociația Culturală „A. Philippide”, Academia Română, Filiala Iași, 23 ianuarie 2014.\n\n* **Metafora în lingvistică. Câteva studii de caz**, Al 13-lea Colocviu Internațional al Departamentului de Lingvistică, Diacronie și sincronie în studiul limbii române, Facultatea de Litere, Universitatea din București, 13-14 decembrie 2013.\n\n* **Metafora în discursul științific**, Simpozionul Internațional «Integrare europeană/identitate națională; plurilingvism/multiculturalitate – limba și cultura română: evaluări, perspective», Institutul de Filologie Română „A. Philippide”, Academia Română – Filiala Iași, 25-26 septembrie 2013.\n\n* **Retorica argumentării în discursul științific. Recursul la metaforă în opera lui Sextil Pușcariu**, Zilele „Sextil Pușcariu”, Ediția I, Institutul de Lingvistică și Istorie Literară „Sextil Pușcariu”, Academia Română, Cluj-Napoca, 12-13 septembrie 2013.\n\n* **Proverbialul Eminescu**, Simpozionul Internațional „Eminescu: Carte-Cultură-Civilizație”, Biblioteca Județeană „Mihai Eminescu”, Botoșani, 14 iunie 2013.\n\n* **Ars dicendi: proverbele românești despre vorbire**, Colocviul „Pedagogia modelelor. Pădurea - ambient decisiv și metaforă primordială”, Putna, 16-19 mai 2013.\n\n* **Reflectarea stereotipurilor etnice în proverbe**, Conferința Internațională de Imagologie «Identitate și alteritate culturală în timp», Universitatea „Sapienția”, Miercurea-Ciuc, 19-20 aprilie 2013.\n\n* **Mozaic și basorelief în publicistica lui Mihai Eminescu**, Simpozionul Internațional „Eminescu: Carte-Cultură-Civilizație”, Biblioteca Județeană „Mihai Eminescu”, Botoșani, 14 iunie 2012.\n\n* **Animal Metaphors in Proverbs**, Conferința Internațională Cognitive Linguistics in the World: Situated and Embodied Approaches, Albert-Ludwigs-Universität, Freiburg im Breisgau, Germania, 10-12 octombrie 2012.\n\n* **Un animal numit putere. Note asupra cîtorva clișee și inovații lingvistice**, Colocviul Național Bestiarul puterii: discursuri, practici, manifestări Facultatea de Filosofie și Științe Social-Politice, Universitatea „Alexandru Ioan Cuza”, Iași, 25-26 octombrie 2012.\n\n* **Imaginarul creștin în denumirile populare românești de plante**, Conferința Națională „Text și discurs religios”, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza”, Iași, 9-10 noiembrie 2012.\n\n* **Despre plante și zmei**, Colocviul Internațional Limba română: variație sincronică, variație diacronică, Facultatea de Litere, Universitatea din București, 14-15 decembrie 2012.\n\n* **The Semantics of Proverbs: A Cognitive Approach**, Colocviul Internaţional Limba română: abordări tradiţionale şi moderne, Facultatea de Litere, Universitatea „Babeş-Bolyai”, Cluj-Napoca, 6-7 mai 2011.\n\n* **Ideologie şi limbaj**, Colocviul Naţional Presa şi ideologia: o istorie culturală, Universitatea „Petre Andrei”, Iaşi, 7-8 aprilie 2011.\n\n* **What do Proverbs Reveal about Language?**, The XIII International Conference Cognitive Modeling in Linguistics, Corfu, Grecia, 22-29 septembrie 2011.\n\n* **Proverbele româneşti despre cruce**, Conferinţa Naţională „Text şi discurs religios”, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza”, Iaşi, 10-12 noiembrie 2011.\n\n* **Creaţia metaforică în argoul românesc: metaforele vegetalului**, Colocviul Internaţional Limba română: direcţii actuale în cercetarea lingvistică, Facultatea de Litere, Universitatea din Bucureşti, 9-10 decembrie 2011.\n\n* **Modele naive şi modele savante în reprezentarea lingvistică a realităţii: numele de plante**, Conferinţa Internatională Limba şi literatura. Repere identitare în context european, Piteşti, 4-6 iunie 2010.\n\n* **Receptarea lui Eminescu, de la mit la kitsch**, Fertilitatea mitului, Putna, 25-28 august 2010.\n\n* **Plant Names: A Cognitive Approach**, The XII International Conference Cognitive Modeling in Linguistics, Dubrovnik, Croatia, 7-14 septembrie 2010.\n\n* **Proverbe şi anti-proverbe**, Al IV-lea Simpozion Internaţional de Lingvistică, Institutul de Lingvistică „Iorgu Iordan-Al. Rosetti”, Bucureşti, 5-6 noiembrie 2010.\n\n* **Norma argotică**, Conferinţa Internaţională Perspective contemporane asupra lumii medievale, Piteşti, 3-5 decembrie 2010.\n\n* **Agresarea numelui în discursul public**, Colocviul Internaţional Limba română: ipostaze ale variaţiei lingvistice, Facultatea de Litere, Universitatea din Bucureşti, 3-4 decembrie 2010.',
						didactic: 'Cursuri și seminarii\n\nLingvistică generală, anul I, studii de licență\n\nLimba română: Stilistică, anul al III-lea, studii de licență\n\nPsiholingvistică actuală, anul al II-lea, studii de masterat\n\nStrategii persuasive în discursul politic şi publicistic, anul al II-lea, studii de masterat',
						administrative: 'Director al Departamentului de Românistică, Jurnalism-Științe ale Comunicării, Literatură comparată\n\nFondator și secretar al Asociației Culturale „Text și discurs religios”\n\nMembru în Consiliul profesoral al Facultății de Litere, Universitatea „Alexandru Ioan Cuza” (din 2012)',
						photo: '/images/professors/ioan-milica.jpg',
						email: 'ioanister@gmail.com',
						password: 'KGhBv3xmSSsDBhVND1jd',
						confirmation: 'KGhBv3xmSSsDBhVND1jd',
						admin: true,
						hidden: false,
					},
					{
						// Alexandru Gafton
						name: 'Alexandru Gafton',
						username: 'alexandru-gafton',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/alexandru-gafton.jpg',
						email: 'alexandru-gafton@romanistica.uaic.ro',
						password: 'KYTc4B5X6xUMTZzwuf3R',
						confirmation: 'KYTc4B5X6xUMTZzwuf3R',
						admin: false,
						hidden: false,
					},
					{
						// Mihaela Secrieru
						name: 'Mihaela Secrieru',
						username: 'mihaela-secrieru',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/mihaela-secrieru.jpg',
						email: 'mihaela-secrieru@romanistica.uaic.ro',
						password: 'Y7M0EFcYwDs9b5HnWIad',
						confirmation: 'Y7M0EFcYwDs9b5HnWIad',
						admin: false,
						hidden: false,
					},
					{
						// Luminița Hoarță Cărăușu
						name: 'Luminița Hoarță Cărăușu',
						username: 'luminita-hoarta-carausu',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/luminita-hoarta-carausu.jpg',
						email: 'luminita-hoarta-carausu@romanistica.uaic.ro',
						password: 'HGSjCQhTYUnK97eHZ3Gp',
						confirmation: 'HGSjCQhTYUnK97eHZ3Gp',
						admin: false,
						hidden: false,
					},
					{
						// Eugen Munteanu
						name: 'Eugen Munteanu',
						username: 'eugen-munteanu',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/eugen-munteanu.jpg',
						email: 'eugen-munteanu@romanistica.uaic.ro',
						password: '3EfTmifMwjooHjzVelGR',
						confirmation: '3EfTmifMwjooHjzVelGR',
						admin: false,
						hidden: false,
					},
					{
						// Sorin Guia
						name: 'Sorin Guia',
						username: 'sorin-guia',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/sorin-guia.jpg',
						email: 'sorin-guia@romanistica.uaic.ro',
						password: 'srwCKAnVuZGUfLFxeuTp',
						confirmation: 'srwCKAnVuZGUfLFxeuTp',
						admin: false,
						hidden: false,
					},
					{
						// Iulia Nica
						name: 'Iulia Nica',
						username: 'iulia-nica',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'iulia-nica@romanistica.uaic.ro',
						password: 'fs7JL8ZmIH1ehMutlpa0',
						confirmation: 'fs7JL8ZmIH1ehMutlpa0',
						admin: false,
						hidden: false,
					},
					{
						// Ana-Maria Minuț
						name: 'Ana-Maria Minuț',
						username: 'ana-maria-minut',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/ana-maria-minut.jpg',
						email: 'ana-maria-minut@romanistica.uaic.ro',
						password: 'Q74sz7APKGZWgQ3jPkk4',
						confirmation: 'Q74sz7APKGZWgQ3jPkk4',
						admin: false,
						hidden: false,
					},
					{
						// Simona Ailenii
						name: 'Simona Ailenii',
						username: 'simona-ailenii',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/simona-ailenii.jpg',
						email: 'simona-ailenii@romanistica.uaic.ro',
						password: 'AB9cunZyhabR8BJ7C8Jw',
						confirmation: 'AB9cunZyhabR8BJ7C8Jw',
						admin: false,
						hidden: false,
					},
					{
						// Roxana Vieru
						name: 'Roxana Vieru',
						username: 'roxana-vieru',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/roxana-vieru.jpg',
						email: 'roxana-vieru@romanistica.uaic.ro',
						password: '5kNGO1T4mHf241IVsKf7',
						confirmation: '5kNGO1T4mHf241IVsKf7',
						admin: false,
						hidden: false,
					},
				],
			},
			{
				name: 'Catedra de Jurnalism şi Ştiinţe ale Comunicării',
				users: [
					{
						// Adrian Hazaparu
						name: 'Adrian Hazaparu',
						username: 'adrian-hazaparu',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/adrian-hazaparu.jpg',
						email: 'adrian-hazaparu@romanistica.uaic.ro',
						password: '5iSavjIS5pLe7Tf5uODn',
						confirmation: '5iSavjIS5pLe7Tf5uODn',
						admin: false,
						hidden: false,
					},
					{
						// Dorin Popa
						name: 'Dorin Popa',
						username: 'dorin-popa',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'dorin-popa@romanistica.uaic.ro',
						password: 'FyISoy3vO6jiQQzKW7a7',
						confirmation: 'FyISoy3vO6jiQQzKW7a7',
						admin: false,
						hidden: false,
					},
					{
						// Alexandru Condurache
						name: 'Alexandru Condurache',
						username: 'alexandru-condurache',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'alexandru-condurache@romanistica.uaic.ro',
						password: 'nqaqT53uHBasL37O3aI2',
						confirmation: 'nqaqT53uHBasL37O3aI2',
						admin: false,
						hidden: false,
					},
					{
						// Violeta Cincu
						name: 'Violeta Cincu',
						username: 'violeta-cincu',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'violeta-cincu@romanistica.uaic.ro',
						password: 'vZBkJRNcmap5kGGTTteE',
						confirmation: 'vZBkJRNcmap5kGGTTteE',
						admin: false,
						hidden: false,
					},
					{
						// Dan S. Stoica
						name: 'Dan S. Stoica',
						username: 'dan-s-stoica',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/dan-stoica.jpg',
						email: 'dan-stoica@romanistica.uaic.ro',
						password: 'V82el0af11UQ98QkZUqR',
						confirmation: 'V82el0af11UQ98QkZUqR',
						admin: false,
						hidden: false,
					},
					{
						// Andrei-Sebastian Stipiuc
						name: 'Andrei-Sebastian Stipiuc',
						username: 'andrei-sebastian-stipiuc',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/andrei-stipiuc.jpg',
						email: 'andrei.stipiuc@gmail.com',
						password: 'UTE1TvreU48fn5AR5nlW',
						confirmation: 'UTE1TvreU48fn5AR5nlW',
						admin: false,
						hidden: false,
					},
					{
						// Florea Ioncioaia
						name: 'Florea Ioncioaia',
						username: 'florea-ioncioaia',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/florea-ioncioaia.jpg',
						email: 'florea-ioncioaia@romanistica.uaic.ro',
						password: 'kXK6TnsCPsJYhB4Ldx8Q',
						confirmation: 'kXK6TnsCPsJYhB4Ldx8Q',
						admin: false,
						hidden: false,
					},
				],
			},
			{
				name: 'Catedra de Literatură comparată',
				users: [
					{
						// Ana-Maria Constantinovici
						name: 'Ana-Maria Constantinovici',
						username: 'ana-maria-constantinovici',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/ana-maria-constantinovici.jpg',
						email: 'anamaria.stefan@uaic.ro',
						password: 'LTjSQCDsRcKGGZS8YNq2',
						confirmation: 'LTjSQCDsRcKGGZS8YNq2',
						admin: false,
						hidden: false,
					},
					{
						// Livia Iacob
						name: 'Livia Iacob',
						username: 'livia-iacob',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/livia-iacob.jpg',
						email: 'liviaiacob@gmail.com',
						password: 'UaVEHsqKvUW0w2lUsgvO',
						confirmation: 'UaVEHsqKvUW0w2lUsgvO',
						admin: false,
						hidden: false,
					},
					{
						// Cătălin Mihai Constantinescu
						name: 'Cătălin Mihai Constantinescu',
						username: 'catalin-mihai-constantinescu',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/catalin-constantinescu.jpg',
						email: 'catalin.constantinescu@uaic.ro',
						password: 'rAsBQw6YdEePgFuojGjg',
						confirmation: 'rAsBQw6YdEePgFuojGjg',
						admin: false,
						hidden: false,
					},
					{
						// Lucia Cifor
						name: 'Lucia Cifor',
						username: 'lucia-cifor',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/lucia-cifor.jpg',
						email: 'luc10for@gmail.com',
						password: 'T5LWmBpvRg5w7f0z9WXy',
						confirmation: 'T5LWmBpvRg5w7f0z9WXy',
						admin: false,
						hidden: false,
					},
					{
						// Mihaela Cernăuţi-Gorodeţchi
						name: 'Mihaela Cernăuţi-Gorodeţchi',
						username: 'mihaela-cernauti-gorodetchi',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/mihaela-cernauti-gorodetchi.jpg',
						email: 'micer@uaic.ro',
						password: 'MrVsSdkA2n8x9GADvBs9',
						confirmation: 'MrVsSdkA2n8x9GADvBs9',
						admin: false,
						hidden: false,
					},
					{
						// Adrian Ioan Crupa
						name: 'Adrian Ioan Crupa',
						username: 'adrian-ioan-crupa',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/adrian-crupa.jpg',
						email: 'adrian.crupa@uaic.ro',
						password: '5DtLyGdeGDgDPvCXkKAe',
						confirmation: '5DtLyGdeGDgDPvCXkKAe',
						admin: false,
						hidden: false,
					},
					{
						// Constantin Dram
						name: 'Constantin Dram',
						username: 'constantin-dram',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/constantin-dram.jpg',
						email: 'constantinxxidram@gmail.com',
						password: 'bA8b6GR9jY8v2eabH7PC',
						confirmation: 'bA8b6GR9jY8v2eabH7PC',
						admin: false,
						hidden: false,
					},
					{
						// Puiu Ioniţă
						name: 'Puiu Ioniţă',
						username: 'puiu-ionita',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/puiu-ionita.jpg',
						email: 'puiu_ionita@yahoo.de',
						password: 'cVRtGv1ozXi4FUcZ1EZS',
						confirmation: 'cVRtGv1ozXi4FUcZ1EZS',
						admin: false,
						hidden: false,
					},
					{
						// Sorin Mocanu
						name: 'Sorin Mocanu',
						username: 'sorin-mocanu',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/sorin-mocanu.jpg',
						email: 'sorin7sorin@gmail.com',
						password: 'QrMAcxuyWZWcxYklArOT',
						confirmation: 'QrMAcxuyWZWcxYklArOT',
						admin: false,
						hidden: false,
					},
				],
			},
			{
				name: 'Catedra de Literatură română',
				users: [
					{
						// Valeriu P. Stancu
						name: 'Valeriu P. Stancu',
						username: 'valeriu-p-stancu',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/valeriu-stancu.jpg',
						email: 'vpstancu@yahoo.com',
						password: '4RX6BRPSKoHKmeOooGVi',
						confirmation: '4RX6BRPSKoHKmeOooGVi',
						admin: false,
						hidden: false,
					},
					{
						// Bogdan Creţu
						name: 'Bogdan Creţu',
						username: 'bogdan-cretu',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/bogdan-cretu.jpg',
						email: 'bogdancretu@yahoo.com',
						password: 'fZrhp3r7CDjgQMTKZKNv',
						confirmation: 'fZrhp3r7CDjgQMTKZKNv',
						admin: false,
						hidden: false,
					},
					{
						// Mircea Păduraru
						name: 'Mircea Păduraru',
						username: 'mircea-paduraru',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'mircea-paduraru@romanistica.uaic.ro',
						password: 'NA25XAoEOotAbq68iNSg',
						confirmation: 'NA25XAoEOotAbq68iNSg',
						admin: false,
						hidden: false,
					},
					{
						// Antonio Patraş
						name: 'Antonio Patraş',
						username: 'antonio-patras',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/antonio-patras.jpg',
						email: 'antonio-patras@romanistica.uaic.ro',
						password: 'c1avZGXKMi1BNCZXlq2U',
						confirmation: 'c1avZGXKMi1BNCZXlq2U',
						admin: false,
						hidden: false,
					},
					{
						// Lăcrămioara Petrescu
						name: 'Lăcrămioara Petrescu',
						username: 'lacramioara-petrescu',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/lacramioara-petrescu.jpg',
						email: 'lacramioara-petrescu@romanistica.uaic.ro',
						password: 'TYk25JheYqFApZSOYrNU',
						confirmation: 'TYk25JheYqFApZSOYrNU',
						admin: false,
						hidden: false,
					},
					{
						// Loredana Cuzmici
						name: 'Loredana Cuzmici',
						username: 'loredana-cuzmici',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/loredana-cuzmici.jpg',
						email: 'lori_opariuc@yahoo.com',
						password: 'A7h51LtgwAvJrRc9Icgc',
						confirmation: 'A7h51LtgwAvJrRc9Icgc',
						admin: false,
						hidden: false,
					},
					{
						// Emanuela Ilie
						name: 'Emanuela Ilie',
						username: 'emanuela-ilie',
						level: 'Conferenţiar universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						// Nu are fotografie.
						email: 'iliemma@yahoo.com',
						password: 'PQqQmIOmCfEf8VVIUyNT',
						confirmation: 'PQqQmIOmCfEf8VVIUyNT',
						admin: false,
						hidden: false,
					},
					{
						// Ilie Moisuc
						name: 'Ilie Moisuc',
						username: 'ilie-moisuc',
						level: 'Asistent universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/ilie-moisuc.jpg',
						email: 'ilie-moisuc@romanistica.uaic.ro',
						password: 'sIYUFxv4bBb9qeNeUf5C',
						confirmation: 'sIYUFxv4bBb9qeNeUf5C',
						admin: false,
						hidden: false,
					},
					{
						// Doris Mironescu
						name: 'Doris Mironescu',
						username: 'doris-mironescu',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/doris-mironescu.jpg',
						email: 'doris-mironescu@romanistica.uaic.ro',
						password: 'AXDjIz6rOO2LKDfGXRk5',
						confirmation: 'AXDjIz6rOO2LKDfGXRk5',
						admin: false,
						hidden: false,
					},
				],
			},
		];
		
		if (count > 0) {
			departments = [];
		}
		
		Department.create(departments).exec(function(err) {
			if (err) {
				return next(err);
			}
			
			Department.find().populateAll().exec(next);
		});
	});
};

var insertProjects = function(next) {
	Project.count().exec(function(err, count) {
		if (err) {
			return next(err);
		}
		
		var projects = [
			{
				name: 'Proiect de cercetare exploratorie I',
				status: 'finalizat',
				start: new Date(),
				end: new Date(),
				stages: 'All stages',
				results: 'All results',
				photo: '/images/professors/zaharia-ciausu.jpg',
				hidden: false,
			},
			{
				name: 'Proiect de cercetare exploratorie II',
				status: 'în desfăşurare',
				start: new Date(),
				end: new Date(),
				stages: 'All stages',
				results: 'All results',
				photo: '/images/professors/zaharia-ciausu.jpg',
				hidden: false,
			},
		];
		
		if (count > 0) {
			projects = [];
		}
		
		Project.create(projects).exec(function(err) {
			if (err) {
				return next(err);
			}
			
			Project.find().populateAll().exec(next);
		});
	});
};

var addNewUsers = function(next) {
	Department.findOne({ name: 'Catedra de Jurnalism şi Ştiinţe ale Comunicării' }, function(err, department) {
		if (err) {
			return next(err);
		}
		
		async.parallel([
			function(next) {
				User.create([
					{
						// Alexandru Lăzescu
						name: 'Alexandru Lăzescu',
						username: 'alexandru-lazescu',
						level: 'Lector universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/alexandru-lazescu.jpg',
						email: 'alexandru-lazescu@romanistica.uaic.ro',
						password: '80Qf1JTXihGUoXEBhQZ2',
						confirmation: '80Qf1JTXihGUoXEBhQZ2',
						department: department.id,
						admin: false,
						hidden: false,
					},
					{
						// Daniel Condurache
						name: 'Daniel Condurache',
						username: 'daniel-condurache',
						level: 'Profesor universitar',
						status: 'Cadru didactic titular',
						scientific: 'Scientific [text aici...]',
						didactic: 'Didactic [text aici...]',
						administrative: 'Administrativ [text aici...]',
						photo: '/images/professors/daniel-condurache.jpg',
						email: 'daniel-condurache@romanistica.uaic.ro',
						password: 'Of45NDqZwTzl48EVaIHx',
						confirmation: 'Of45NDqZwTzl48EVaIHx',
						department: department.id,
						admin: false,
						hidden: false,
					},
				], next);
			},	
		], next);
	});
};

var updateUsersPhotos = function(next) {
	console.log('Updating...');
	
	async.parallel([
		function(next) {
			console.log('Updating Violeta Cincu');
			User.update({username: 'violeta-cincu'}, {photo: '/images/professors/violeta-cincu.jpg'}, next);	
		},
		function(next) {
			console.log('Updating Mircea Paduraru');
			User.update({username: 'mircea-paduraru'}, {photo: '/images/professors/mircea-paduraru.jpg'}, next);	
		},
		function(next) {
			console.log('Updating Emanuela Ilie');
			User.update({username: 'emanuela-ilie'}, {photo: '/images/professors/emanuela-ilie.jpg'}, next);	
		},
		function(next) {
			console.log('Updating Dorin Popa');
			User.update({username: 'dorin-popa'}, {photo: '/images/professors/dorin-popa.jpg'}, next);	
		},
	], next);
};

var updateAlexandruCondurachePhoto = function(next) {
	console.log('Updating...');
	
	async.parallel([
		function(next) {
			console.log('Updating Alexandru Condurache');
			User.update({username: 'alexandru-condurache'}, {photo: '/images/professors/alexandru-condurache.jpg'}, next);	
		},
	], next);
};

var deleteDanStoica = function(next) {
	User.destroy({username: 'dan-s-stoica'}, next);
};

var deleteDanielCondurache = function(next) {
	User.destroy({username: 'daniel-condurache'}, next);
};

var updateBogdanCretuProfessor = function(next) {
	User.update({username: 'bogdan-cretu'}, {level: 'Profesor universitar'}, next);	
};

var updateIlieMoisucLecturer = function(next) {
	User.update({username: 'ilie-moisuc'}, {level: 'Lector universitar'}, next);	
};

var updateGinaNimigeanLecturer = function(next) {
	User.update({username: 'gina-nimigean'}, {level: 'Lector universitar'}, next);
};

var updateMirceaPaduraruLecturer = function(next) {
	User.update({username: 'mircea-paduraru'}, {level: 'Lector universitar'}, next);
};

var deleteDanielCondurache = function(next) {
	User.destroy({username: 'daniel-condurache'}, next);
};

var deleteAlexandruCondurache = function(next) {
	User.destroy({username: 'alexandru-condurache'}, next);
};

var deleteIoanaMoldovanuCenusa = function(next) {
	User.destroy({username: 'ioana-moldovanu-cenusa'}, next);
};

var deleteVioletaCincu = function(next) {
	User.destroy({username: 'violeta-cincu'}, next);
};

var deleteVladinaMunteanu = function(next) {
	User.destroy({username: 'vladina-munteanu'}, next);
};

var deleteZahariaCeausu = function(next) {
	User.destroy({username: 'zaharia-ciausu'}, next);
};

var insertCodrutaAntonesei = function(next) {
	Department.findOne({ name: 'Catedra de Jurnalism şi Ştiinţe ale Comunicării' }, function(err, department) {
		if (err) {
			return next(err);
		}

		User.create([
			{
				// Codruța Antonesei
				name: 'Codruța Antonesei',
				username: 'codruta-antonesei',
				level: 'Lector universitar',
				status: 'Cadru didactic titular',
				scientific: 'Scientific [text aici...]',
				didactic: 'Didactic [text aici...]',
				administrative: 'Administrativ [text aici...]',
				// Nu are fotografie.
				email: 'codruta-antonesei@romanistica.uaic.ro',
				password: 'CjITEzO9Ol764ju7',
				confirmation: 'CjITEzO9Ol764ju7',
				department: department.id,
				admin: false,
				hidden: false,
			}
		], next);
	});
};

var updateNamesAndEmails = function(next) {
	async.parallel([
		function(next) {
			User.update({username: 'luminita-hoarta-carausu'}, {email: 'lumicarausu@yahoo.com', name: 'Luminița Mirela Cărăușu'}, next);
		},
		function(next) {
			User.update({username: 'mihaela-cernauti-gorodetchi'}, {email: 'micer@uaic.ro'}, next);
		},
		function(next) {
			User.update({username: 'lucia-cifor'}, {email: 'luciacifor@yahoo.com', name: 'Lucica Chifor'}, next);
		},
		function(next) {
			User.update({username: 'constantin-dram'}, {email: 'constantinxxidram@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'alexandru-gafton'}, {email: 'algafton@gmail.com', name: 'Alexandru Arnold Francisc Gafton'}, next);
		},
		function(next) {
			User.update({username: 'eugen-munteanu'}, {email: 'eugenmunteanu@hotmail.com'}, next);
		},
		function(next) {
			User.update({username: 'antonio-patras'}, {email: 'patrasantonio@gmail.com', name: 'Antonio Mihail Patraș'}, next);
		},
		function(next) {
			User.update({username: 'lacramioara-petrescu'}, {email: 'petrescu@uaic.ro', name: 'Dorina Lăcrămioara Petrescu'}, next);
		},
		function(next) {
			User.update({username: 'mihaela-secrieru'}, {email: 'msecrieru@yahoo.com', name: 'Mihaela Lenuța Secrieru'}, next);
		},
		function(next) {
			User.update({username: 'bogdan-cretu'}, {email: 'bogdancretu@yahoo.com', name: 'Bogdan Mihai Crețu'}, next);
		},
		function(next) {
			User.update({username: 'dorin-popa'}, {email: 'dorinpopa9@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'ludmila-braniste'}, {email: 'branisteludmila@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'catalin-mihai-constantinescu'}, {email: 'c.constantinescu@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'emanuela-ilie'}, {email: 'iliemma@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'florea-ioncioaia'}, {email: 'ioncioaia@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'ana-maria-minut'}, {email: 'a_minut@hotmail.com'}, next);
		},
		function(next) {
			User.update({username: 'valeriu-p-stancu'}, {email: 'vpstancu@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'iolanda-sterpu'}, {email: 'i_sterpu@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'ioan-milica'}, {email: 'mioan@uaic.ro'}, next);
		},
		function(next) {
			User.update({username: 'sorin-guia'}, {email: 'soringuia01@yahoo.com', name: 'Sorin Grigore Guia'}, next);
		},
		function(next) {
			User.update({username: 'codruta-antonesei'}, {email: 'codrutant@yahoo.fr'}, next);
		},
		function(next) {
			User.update({username: 'ana-maria-constantinovici'}, {email: 'anamaria.stefan@uaic.ro'}, next);
		},
		function(next) {
			User.update({username: 'adrian-ioan-crupa'}, {email: 'adrian.crupa@uaic.ro'}, next);
		},
		function(next) {
			User.update({username: 'livia-iacob'}, {email: 'liviaiacob@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'alexandru-lazescu'}, {email: 'alexandru.lazescu@gmail.com', name: 'Alexandru Mihai Lăzescu'}, next);
		},
		function(next) {
			User.update({username: 'doris-mironescu'}, {email: 'dorismironescu@yahoo.com', name: 'Doris Dumitru Mironescu'}, next);
		},
		function(next) {
			User.update({username: 'sorin-mocanu'}, {email: 'sorin7sorin@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'iulia-nica'}, {email: 'iulianica@yahoo.com', name: 'Mihaela Iuliana Nica'}, next);
		},
		function(next) {
			User.update({username: 'loredana-cuzmici'}, {email: 'loricuzmici@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'adrian-hazaparu'}, {email: 'adihazaparu@yahoo.com', name: 'Marius Adrian Hazaparu'}, next);
		},
		function(next) {
			User.update({username: 'ilie-moisuc'}, {email: 'ilie_moisuc@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'mircea-paduraru'}, {email: 'mircea_paduraru@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'roxana-vieru'}, {email: 'rxn_x@yahoo.com'}, next);
		},
		function(next) {
			User.update({username: 'simona-ailenii'}, {email: 'aileneisimonasid@gmail.com', name: 'Simona Muraru'}, next);
		},
		function(next) {
			User.update({username: 'andrei-sebastian-stipiuc'}, {email: 'andrei.stipiuc@gmail.com'}, next);
		},
		function(next) {
			User.update({username: 'carmen-livia-tudor'}, {email: 'carmen.livia@yahoo.com'}, next);
		},
	], next);
};

var deletePuiuIonita = function(next) {
	User.destroy({username: 'puiu-ionita'}, next);
};

var updateSimonaAileniiName = function(next) {
	User.update({username: 'simona-ailenii'}, {name: 'Simona Ailenii'}, next);
};

var updateGinaNimigeanEmail = function(next) {
	User.update({username: 'gina-nimigean'}, {email: 'ginanimigean@yahoo.com'}, next);	
};

var users = [{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Antonio Patraş","username":"antonio-patras","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]1111","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/antonio-patras.jpg","email":"antonio-patras@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.252Z","updatedAt":"2015-10-03T17:11:04.516Z","id":"55ec67ddaae1a18a090319fa"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Bogdan Creţu","username":"bogdan-cretu","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/bogdan-cretu.jpg","email":"bogdancretu@yahoo.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.256Z","updatedAt":"2015-09-06T16:20:45.256Z","id":"55ec67ddaae1a18a090319fc"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Valeriu P. Stancu","username":"valeriu-p-stancu","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/valeriu-stancu.jpg","email":"vpstancu@yahoo.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.261Z","updatedAt":"2015-09-06T16:20:45.261Z","id":"55ec67ddaae1a18a090319fd"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Lăcrămioara Petrescu","username":"lacramioara-petrescu","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/lacramioara-petrescu.jpg","email":"lacramioara-petrescu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.326Z","updatedAt":"2015-09-06T16:20:45.326Z","id":"55ec67ddaae1a18a090319fe"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Loredana Cuzmici","username":"loredana-cuzmici","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/loredana-cuzmici.jpg","email":"lori_opariuc@yahoo.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.328Z","updatedAt":"2015-09-06T16:20:45.328Z","id":"55ec67ddaae1a18a090319ff"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Ilie Moisuc","username":"ilie-moisuc","level":"Asistent universitar","status":"Cadru didactic titular\n\nConducător de lucrări de licență","scientific":"[*Forme ale identificării lectorale în **Amintiri din copilărie***][1]\n\n\n  [1]: http://www.putna.ro/pdf/CP-5-2012.pdf","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/ilie-moisuc.jpg","email":"ilie-moisuc@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.342Z","updatedAt":"2015-10-27T06:20:15.931Z","id":"55ec67ddaae1a18a09031a01"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Vladina Munteanu","username":"vladina-munteanu","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"vladina-munteanu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.477Z","updatedAt":"2015-09-06T16:20:45.477Z","id":"55ec67ddaae1a18a09031a07"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Doris Mironescu","username":"doris-mironescu","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/doris-mironescu.jpg","email":"doris-mironescu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.400Z","updatedAt":"2015-09-06T16:20:45.400Z","id":"55ec67ddaae1a18a09031a02"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Zaharia Ciaușu","username":"zaharia-ciausu","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/zaharia-ciausu.jpg","email":"zaharia-ceausu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.403Z","updatedAt":"2015-09-06T16:20:45.403Z","id":"55ec67ddaae1a18a09031a03"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Ioana Moldovanu-Cenușă","username":"ioana-moldovanu-cenusa","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"ioana-moldovanu-cenusa@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.412Z","updatedAt":"2015-09-06T16:20:45.412Z","id":"55ec67ddaae1a18a09031a04"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Carmen Livia Tudor","username":"carmen-livia-tudor","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/carmen-livia-tudor.jpg","email":"carmen-livia-tudor@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.440Z","updatedAt":"2015-09-06T16:20:45.440Z","id":"55ec67ddaae1a18a09031a05"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Gina Nimigean","username":"gina-nimigean","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/gina-nimigean.jpg","email":"gina-nimigean@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.475Z","updatedAt":"2015-09-06T16:20:45.475Z","id":"55ec67ddaae1a18a09031a06"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Iolanda Sterpu","username":"iolanda-sterpu","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/iolanda-sterpu.jpg","email":"iolanda-sterpu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.484Z","updatedAt":"2015-09-06T16:20:45.484Z","id":"55ec67ddaae1a18a09031a08"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Ana-Maria Constantinovici","username":"ana-maria-constantinovici","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/ana-maria-constantinovici.jpg","email":"anamaria.stefan@uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.549Z","updatedAt":"2015-09-06T16:20:45.549Z","id":"55ec67ddaae1a18a09031a0a"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Livia Iacob","username":"livia-iacob","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/livia-iacob.jpg","email":"liviaiacob@gmail.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.551Z","updatedAt":"2015-09-06T16:20:45.551Z","id":"55ec67ddaae1a18a09031a0b"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Cătălin Mihai Constantinescu","username":"catalin-mihai-constantinescu","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/catalin-constantinescu.jpg","email":"catalin.constantinescu@uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.558Z","updatedAt":"2015-09-06T16:20:45.558Z","id":"55ec67ddaae1a18a09031a0c"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Lucia Cifor","username":"lucia-cifor","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/lucia-cifor.jpg","email":"luc10for@gmail.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.611Z","updatedAt":"2015-09-06T16:20:45.611Z","id":"55ec67ddaae1a18a09031a0d"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Mihaela Cernăuţi-Gorodeţchi","username":"mihaela-cernauti-gorodetchi","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/mihaela-cernauti-gorodetchi.jpg","email":"micer@uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.624Z","updatedAt":"2015-09-06T16:20:45.624Z","id":"55ec67ddaae1a18a09031a0e"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Adrian Ioan Crupa","username":"adrian-ioan-crupa","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/adrian-crupa.jpg","email":"adrian.crupa@uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.626Z","updatedAt":"2015-09-06T16:20:45.626Z","id":"55ec67ddaae1a18a09031a0f"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Constantin Dram","username":"constantin-dram","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/constantin-dram.jpg","email":"constantinxxidram@gmail.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.632Z","updatedAt":"2015-09-06T16:20:45.632Z","id":"55ec67ddaae1a18a09031a10"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Puiu Ioniţă","username":"puiu-ionita","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/puiu-ionita.jpg","email":"puiu_ionita@yahoo.de","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.692Z","updatedAt":"2015-09-06T16:20:45.692Z","id":"55ec67ddaae1a18a09031a11"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură comparată","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.090Z","id":"55ec67dcaae1a18a090319f8"},"name":"Sorin Mocanu","username":"sorin-mocanu","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/sorin-mocanu.jpg","email":"sorin7sorin@gmail.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.698Z","updatedAt":"2015-09-06T16:20:45.698Z","id":"55ec67ddaae1a18a09031a12"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Iulia Nica","username":"iulia-nica","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"iulia-nica@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.847Z","updatedAt":"2015-09-06T16:20:45.847Z","id":"55ec67ddaae1a18a09031a19"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Adrian Hazaparu","username":"adrian-hazaparu","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/adrian-hazaparu.jpg","email":"adrian-hazaparu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.935Z","updatedAt":"2015-09-06T16:20:45.935Z","id":"55ec67ddaae1a18a09031a1f"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Alexandru Condurache","username":"alexandru-condurache","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"alexandru-condurache@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.959Z","updatedAt":"2015-10-04T22:54:26.727Z","photo":"/images/professors/alexandru-condurache.jpg","id":"55ec67ddaae1a18a09031a20"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Dan S. Stoica","username":"dan-s-stoica","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/dan-stoica.jpg","email":"dan-stoica@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:46.009Z","updatedAt":"2015-09-06T16:20:46.009Z","id":"55ec67deaae1a18a09031a22"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Andrei-Sebastian Stipiuc","username":"andrei-sebastian-stipiuc","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/andrei-stipiuc.jpg","email":"andrei.stipiuc@gmail.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:46.011Z","updatedAt":"2015-09-06T16:20:46.011Z","id":"55ec67deaae1a18a09031a23"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Florea Ioncioaia","username":"florea-ioncioaia","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/florea-ioncioaia.jpg","email":"florea-ioncioaia@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:46.033Z","updatedAt":"2015-09-06T16:20:46.033Z","id":"55ec67deaae1a18a09031a24"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Daniel Condurache","username":"daniel-condurache","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/daniel-condurache.jpg","email":"daniel-condurache@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-10-04T22:27:12.243Z","updatedAt":"2015-10-04T22:27:12.243Z","id":"5611a7c05aeaef1628eedc5f"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Alexandru Lăzescu","username":"alexandru-lazescu","level":"Lector universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","photo":"/images/professors/alexandru-lazescu.jpg","email":"alexandru-lazescu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-10-04T22:27:12.231Z","updatedAt":"2015-10-04T22:27:12.231Z","id":"5611a7c05aeaef1628eedc5d"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Violeta Cincu","username":"violeta-cincu","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"violeta-cincu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:46.007Z","updatedAt":"2015-10-04T22:49:41.255Z","photo":"/images/professors/violeta-cincu.jpg","id":"55ec67deaae1a18a09031a21"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Mircea Păduraru","username":"mircea-paduraru","level":"Asistent universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"mircea-paduraru@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.254Z","updatedAt":"2015-10-04T22:49:41.263Z","photo":"/images/professors/mircea-paduraru.jpg","id":"55ec67ddaae1a18a090319fb"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Jurnalism şi Ştiinţe ale Comunicării","hidden":false,"createdAt":"2015-09-06T16:20:44.240Z","updatedAt":"2015-09-06T16:20:45.088Z","id":"55ec67dcaae1a18a090319f7"},"name":"Dorin Popa","username":"dorin-popa","level":"Profesor universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"dorin-popa@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.934Z","updatedAt":"2015-10-04T22:49:41.266Z","photo":"/images/professors/dorin-popa.jpg","id":"55ec67ddaae1a18a09031a1e"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Literatură română","hidden":false,"createdAt":"2015-09-06T16:20:44.241Z","updatedAt":"2015-09-06T16:20:45.074Z","id":"55ec67dcaae1a18a090319f9"},"name":"Emanuela Ilie","username":"emanuela-ilie","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"Scientific [text aici...]","didactic":"Didactic [text aici...]","administrative":"Administrativ [text aici...]","email":"iliemma@yahoo.com","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.332Z","updatedAt":"2015-10-04T22:49:41.266Z","photo":"/images/professors/emanuela-ilie.jpg","id":"55ec67ddaae1a18a09031a00"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Ioan Milică","username":"ioan-milica","level":"Conferenţiar universitar","status":"Cadru didactic titular\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nCoordonator al colecției\n[***Logos***][1], Editura Universității „Alexandru Ioan Cuza”.\n\nEditor al anuarului [***Text și discurs religios***][2].\n\nMembru (secretar de redacție) în colegiul editorial al revistei ***Analele Științifice ale Universității „Alexandru Ioan Cuza din Iași***, secţiunea III e, ***Lingvistică***.\n\nMembru în colegiul științific al revistei [***Limba română***][3], Chișinău\n\nMembru în comitetul științific al revistei [***Argotica***. Revistă internațională de studii argotice][4]\n\nReferent ştiinţific al revistei [***Limbă şi Literatură. Repere identitare în context european***][5], Facultatea de Litere, Universitatea din Piteşti\n\n\n  [1]: http://www.editura.uaic.ro/colectii.php?colectie=logos\n  [2]: http://www.cntdr.ro/revista\n  [3]: http://limbaromana.md/?go=autori&m=378\n  [4]: http://cis01.central.ucv.ro/litere/argotica/meniu/ro/3.%20Comitet_stiintific.pdf\n  [5]: http://www.upit.ro/uploads/facultatea_lit/ELI/editorial%20board%20site%20ELI.pdf","scientific":"# Vizibilitatea cercetării\n\nPortalul [***academia.edu***][1]\n\n[***Google Academic***][2]\n\nBaza de date [***Diacronia***][3]\n\n# Volume\n\n* [**Expresivitatea argoului**][4], Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2009\n\n* [**Lumi discursive**][5], Editura Junimea, Iași, 2013\n\n* [**Noțiuni de stilistică**][6], Editura Vasiliana ‘98, 2014\n\n# Ediții\n\n* Dumitru Irimia, [**Curs de lingvistică generală**][7], postfață de Ilie Moisuc, ediţia a III-a, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2011\n\n* Dumitru Irimia, [**Studii eminesciene**][8], Editura Universității „Alexandru Ioan Cuza” din Iaşi, 2014 (co-editor: Ilie Moisuc)\n\n# Colaborare la lucrări de referință\n\n* Marinescu, Cornelia (coord.), **Enciclopedia Concisă Britannica**, București, Editura Litera, 2009\n\n* Câmpeanu, Ilieș, Marinescu, Cornelia (coord.), **Enciclopedia Universală Britannica**, 16 volume, București, Editura Litera, 2010\n\n# Volume ale unor manifestări științifice\n\n* **Perspective asupra textului și discursului religios**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2013 (co-editori: Emanuel Gafton, Sorin Guia)\n\n* **Limba română azi**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2007 (editor coordonator: Dumitru Irimia, co-editor: Ana-Maria Minuț)\n\n# Articole (selectiv)\n\n* ***Proverbes et anti-proverbes***, **Philologica Jassyensia**, ISSN 1841-3347, Editura Alfa, Iași, anul IX, nr. 1/2013, p. 63-79.\n\n* ***Plant Names: A Cognitive Approach***, în Vladimir Polyakov, Valery Solovyev (eds.), **Cognitive Modeling in Linguistics**, Cambridge Scholars Publishing, Newcastle upon Tyne, p. 105-120.\n\n* [***Notes on the Semantics of the Romanian cruce: from Lexis to Proverbs***][9], **Text și discurs religios**, Editura Universității „Alexandru Ioan Cuza”, Iași, nr. 4, 2012, p. 319-331.\n\n* [***Christian Imagery in Romanian Folk Plant Names***][10], **Text și discurs religios**, Editura Universității „Alexandru Ioan Cuza”, Iași, nr. 5/2013, p. 313-334.\n\n* ***Naive and Expert Models in the Linguistic Representation of Reality: the Names of Plants***, **Language and Literature. European Landmarks of Identity**, nr. 6, 2010, Editura Universităţii din Piteşti, p. 70-78.\n\n* ***L’agression des noms dans la communication par le biais de l’Internet***, **Studii şi Cercetări de Onomastică şi Lexicologie**, Editura Sitech, Craiova, an III, nr. 1-2, 2010, p. 168-179.\n\n* [***Grammaticalisation et expressivité***][11], **Revue Roumaine de Linguistique**, Editura Academiei Române, Bucureşti, nr. 3 (LV), 2010, p. 301-312.\n\n\n  [1]: https://uaic.academia.edu/IoanMilica\n  [2]: https://scholar.google.ro/citations?user=9dlhYLQAAAAJ&hl=ro\n  [3]: http://www.diacronia.ro/en/profile/IoanMilica\n  [4]: http://www.editura.uaic.ro/fisa-carte.php?id_d=d05&id_c=811\n  [5]: http://www.editurajunimea.ro/book/?id=379\n  [6]: http://www.librarie.net/p/220710/Notiuni-stilistica\n  [7]: http://www.editura.uaic.ro/fisa-carte.php?id_d=d05&id_c=976\n  [8]: http://www.editura.uaic.ro/fisa-carte.php?id_d=d05&id_c=1238\n  [9]: http://www.cntdr.ro/sites/default/files/c2011/c2011a29.pdf\n  [10]: http://www.cntdr.ro/sites/default/files/c2012/c2012a27.pdf\n  [11]: http://www.lingv.ro/RRL%203%202010%20art07Milica.pdf","didactic":"* **Lingvistică generală**, anul I, studii de licență\n\n* **Limba română: Stilistică**, anul al III-lea, studii de licență\n\n* **Psiholingvistică actuală**, anul al II-lea, studii de masterat\n","administrative":"Director al **Departamentului de Românistică, Jurnalism-Științe ale Comunicării și Literatură comparată**\n\nFondator și secretar al **Asociației Culturale „Text și discurs religios”**\n\nMembru în **Consiliul profesoral al Facultății de Litere**, Universitatea „Alexandru Ioan Cuza” (din 2012)","photo":"/images/professors/ioan-milica.jpg","email":"ioanister@gmail.com","admin":true,"hidden":false,"createdAt":"2015-09-06T16:20:45.712Z","updatedAt":"2015-10-27T06:16:53.714Z","contact":"![enter image description here][1]\n\n\nO poză mai sus :)\n\nModalităţi de contact - listă\n\n 1. Email - [email ascuns, daţi click!][2]\n 2. List item\n\n> A fost odata ca-n povesti - citat\n\n----------\n\n - O lista neordonata, ...\n - Inca un item...\n\n# Antet 1\n## Antet 2\n### Antet 3\n#### Antet 4\n##### Antet 5\n###### Antet 6\n\n\n  [1]: http://www.linghams.co.uk/wp-content/uploads/2014/02/open_book_on_tabl_450.jpg\n  [2]: http://mailto:ioanister@gmail.com","id":"55ec67ddaae1a18a09031a14"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Alexandru Gafton","username":"alexandru-gafton","level":"Profesor universitar","status":"Cadru didactic titular\n\nConducător de lucrări de doctorat\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nEditor al anuarului [***Text și discurs religios***][1]\n\nFondator și editor-șef al revistei [***Diacronia***][2]\n\nMembru în colegiul editorial al revistei ***Analele Științifice ale Universității „Alexandru Ioan Cuza din Iași***, secţiunea III e, ***Lingvistică***.\n\n  [1]: http://www.cntdr.ro/revista\n  [2]: http://www.diacronia.ro/en","scientific":"# Volume\n* **Hipercorectitudinea**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2000\n\n* **Elemente de istoria limbii române**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2000\n\n* **Evoluția limbii române prin traduceri biblice în secolul al XVI-lea**. *Studiu lingvistic asupra Codicelui Bratul în comparație cu Codicele Voronețean, Praxiul Coresian și Apostolul Iorga*, Editura Universității „Alexandru Ioan Cuza”, Iași, 2001\n\n* **Introducere în paleografia româno-chirilică**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2001 (ediția a doua, 2003)\n\n* **După Luther. Edificarea normei literare românești prin traduceri biblice**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2005\n\n* **Palia de la Orăștie (1582)**, II, **Studii** (în colaborare cu V. Arvinte), Editura Universității „Alexandru Ioan Cuza”, Iași, 2007\n\n* **De la traducere la norma literară**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2012\n\n#Ediții\n* **Codicele Bratul**, ediție de text și studiu filologic, Editura Universității „Alexandru Ioan Cuza”, Iași, 2003\n\n* **Biblia 1688**, Editura Universității „Alexandru Ioan Cuza”, 2 vol., Iași, 2001, 2002 (în colaborare cu V. Arvinte, I, Caproșu, Laura Manea, N.A. Ursu)\n\n* **Palia de la Orăștie (1582)**, I, **Textul**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2005 (în colaborare cu V. Arvinte, I. Caproșu, S. Guia)\n\n#Traduceri\n\n* **Creștinismul antic**, Casa Editorială Demiurg Plus, Iași, 2011 (Charles Guignebert, **Le christianisme antique**, Ernest Flammarion, Paris, 1921)\n\n* **Schiță pentru o istorie a limbii latine**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2014, co-traducător Iulian Popescu (Antoine Meillet, **Esquisse d’une histoire de la langue latine**, Hachette, Paris, 1928)\n\n* **Manual de lingvistică romanică**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2014 (Martin Dietrich Glessgen, **Manuel de linguistique romane**, Armand Colin, Paris, 2012)\n\n* **De la greci la Darwin**. *Schiță a dezvoltării ideii de evoluție*, Casa Editorială Demiurg, Iași, 2014, co-traducător Adina Chirilă (Henry Fairfield Osborn, From the Greeks to Darwin. An Outline of the Development of the Evolution Idea, The MacMillan Company, London, New York, 1913)\n\n* **Darwin și științele umaniste**, Casa Editorială Demiurg, Iași, 2015, co-traducător Adina Chirilă (James Mark Baldwin, **Darwin and the Humanities**, Review Publishing Co., Baltimore, 1909)\n\n* **Viața cuvintelor studiată în semnificațiile lor**, Editura Universității „Alexandru Ioan Cuza” (Arsène Darmesteter, **La vie des mots étudiée dans leur significations**, Librairie Ch. Delagrave, Paris, 1895)\n\n# Articole (selectiv)\n\n* ***Relaţia dintre sursele traducerilor biblice şi concepţia de la baza acestora***, în **Text şi discurs religios**, 1/2009, p. 125-134\n\n* ***Originea Romînilor - piatră de temelie a Şcolii lingvistice de la Iaşi***, în „Philologica Jassyensia”, an V, nr. 1 (9), 2009, p. 51-55\n\n* ***Traducerea ca formă de achiziţie a conceptelor şi mentalităţilor***, în **Tabor**, nr. 2, anul III, mai 2009, Cluj, p. 52-57\n\n* ***La traduction de texte sacré: entre contraintes et libertés***, în **Actes de Ier Colloque International Perspectives contemporaines sur le monde médieval**, nr. 1/2009, p. 29-32\n\n* ***Biblia de la 1688. Aspecte ale traducerii***, în **Text şi discurs religios**, 2/2010, p. 49-72\n\n* ***Traducerea ca literă şi glosa ca spirit***, în **Tabor**, nr. 4, anul IV, iulie 2010, p. 53-61\n\n* ***Consecinţele profunde ale contactelor lingvistice***, în **Studii de limba română. Omagiu profesorului Grigore Brâncuş**, ed. Gh. Chivu, Oana Uţă-Bărbulescu, Bucureşti, 2010, p. 77-100\n\n* ***La traduction en tant que lettre et la glosse en tant qu’esprit***, în **Langue et littérature. Repères identitaires en contexte européen**, Piteşti, 2010, p. 7-16\n\n* ***Principiul diacronic în edificarea normei literare***, în **Limba română: controverse, delimitări, noi ipoteze, actele celui de-al 9-lea colocviu al catedrei de limba română**, vol. I, Bucureşti, 2010, p. 347-353\n\n* ***Asupra unei traduceri din Noul Testament de la Bălgrad (1648)***, în **Text şi discurs religios**, 3/2011, p. 205-207\n\n* ***Particularităţi ale traducerii în Biblia de la Bucureşti şi în Noul Testament de la Bălgrad. Cu ilustrări din Epistola lui Iacov***, în **Limba română** LX, 2011, nr. 2 aprilie-iunie, p. 261-272\n\n* ***Donc***, în **Langue et littérature. Repères identitaires en contexte européen**, Piteşti, 2011, p. 15-21\n\n* ***Asupra unei traduceri din Biblia de la 1688***, în **Filologie şi bibliologie, In Honorem Vasile D. Ţâra**, Timişoara, 2011, p. 207-211\n\n* ***Termes appartenant au champ semantique ‘eduquer’. Une perspective diachronique sur le processus***, în **In Magistri Honorem Vasile Frăţilă, 50 de ani de carieră universitară**, Tîrgu-Mureş, 2012, p. 223-245\n\n* ***Reflectarea tensiunii cultural / cultual în procesul de declarare a surselor textului biblic. Cazul vechilor traduceri româneşti***, în **AUI** LVIII (2012), p. 127-153\n\n* ***Dubletele sinonimice în discursul religios***, în **AUI** LVIII (2012), p. 155-178 (coautor I. Milică)\n\n* ***Uzul, valorile şi dispariţia unor termeni: făţărie, a făţări, făţărnicie, a făţărnici, făţarnic***, în **Limba română**, LXI, 2012, nr. 2, aprilie-iunie, p. 192-204\n\n* ***Sources déclarées et sources réelles. Le cas des anciennes traductions roumaines de la Bible***, în **Synergies Roumanie**, 7/2012, p. 257-284\n\n* ***Les avatars conceptuels de la famille du roum. faţă. Le témoignage des traductions de la Bible***, în **Revue de linguistique romane**, nr. 305-306, tome 77 (janvier-juin 2013), p. 183-201","didactic":"* **Istoria limbii române**, anul al II-lea, studii de licență\n\n* **Tehnica editării textelor vechi**, anul I, studii de masterat\n\n* **Lingvistică diacronică**, anul al II-lea, studii de masterat\n\n* **Doctrine lingvistice moderne**, curs modular, anul I, Școala Doctorală de Studii Filologice, Facultatea de Litere, \nUniversitatea „Alexandru Ioan Cuza” din Iași","administrative":"Șef al **Catedrei de Limbă română și Lingvistică generală** (2006-2014)\n\nEvaluator pentru domeniul *Limbă și literatură*, **Agenția Română de Asigurare a Calității în Învățământul Superior** (ARACIS)\n\nPreședinte al **Comisiei de Filologie, Consiliul Național de Atestare a Titlurilor, Diplomelor și Certificatelor Universitare** (CNATDCU)\n\nFondator și vicepreședinte al **Asociației Culturale** ***Text și discurs religios***\n","photo":"/images/professors/alexandru-gafton.jpg","email":"alexandru-gafton@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.707Z","updatedAt":"2015-10-05T05:39:59.537Z","contact":"#E-mail\nalgafton@gmail.com","id":"55ec67ddaae1a18a09031a13"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Eugen Munteanu","username":"eugen-munteanu","level":"Profesor universitar","status":"Cadru didactic titular\n \nConducător de lucrări de doctorat\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nDirector al **Centrului de Studii Biblico-Filologice** [***Monumenta Linguae Dacoromanorum***][1]\n\nInițiator și coordonator al colecției [***Fontes Traditionis***][2] Editura Universității „Alexandru Ioan Cuza”\n\nEditor al revistei [***Biblicum Jassyense. Romanian Journal for Biblical Philology and Hermeneutics***][3]\n\nMembru în colegiul științific al revistei  [***Limba română***][4], Chișinău\n\nMembru în comitetul științific al revistei [***Philologica Jassyensia***][5]\n\nMembru în colegiul editorial al revistei [***Anuar de lingvistică și istorie literară***][6]\n\nMembru în colegiul editorial al revistei [***Zeitschrift für Romanische Philologie***][7]\n\n\n  [1]: http://consilr.info.uaic.ro/~mld/monumenta/\n  [2]: http://www.editura.uaic.ro/colectii.php?colectie=fontes_traditionis\n  [3]: http://consilr.info.uaic.ro/~mld/monumenta/revista.html\n  [4]: http://limbaromana.md/?go=autori&m=45\n  [5]: http://www.philologica-jassyensia.ro/comitet.html\n  [6]: http://www.alil.ro/?page_id=6\n  [7]: http://www.degruyter.com/view/j/zrph","scientific":"# Volume \n\n* **Studii de lexicologie biblică**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 1995\n\n* **Aeterna Latinitas. Mică enciclopedie a gîndirii europene în expresie latină**, Editura Polirom, Iaşi, 1997 (în colaborare cu Lucia Gabriela Munteanu)\n\n* **Introducere în lingvistică**, Editura Polirom, Iaşi, 2005\n\n* **Lexicologie biblică românească**, Editura Humanitas, Bucureşti, 2008\n\n#Ediții\n* Sf. Augustin, **De dialectica / Despre dialectică**. Ediţie bilingvă, introducere, note, comentarii şi bibliografie, Editura “Humanitas\", Bucureşti, 1991\n\n* **Omul şi limbajul său. Studia linguistica in honorem Eugenio Coseriu**, Iaşi, Editura Universităţii, 1992, (coeditor alături de Dumitru Irimia)\n\n* Eugen Coşeriu, **Prelegeri şi conferinţe**. Coeditor alături de Carmen-Gabriela Pamfil, Ioan Oprea şi Adrian Turculeţ, în ANLL (1992-1993), Iaşi, 1994\n\n* Sfîntul Augustin, **De Magistro / Despre Învăţător**. Ediţie bilingvă, traducere, introducere, comentarii, note şi bibliografie, Institutul European, Iaşi, 1995\n\n* **Carmina Burana**. Ediţie bilingvă comentată, Editura Polirom, Iaşi, 1998 (în colaborare cu Lucia Gabriela Munteanu\n\n* Thomas de Aquino, **De ente et essentia / Despre fiind şi esenţă**. Ediţie bilingvă, traducere, introducere, note şi comentarii, Editura Polirom, Iaşi, 1998\n\n* Jean Jaques Rousseau, **Eseu despre originea limbajului**. Traducere, introducere, note şi comentarii, Editura “Polirom\", Iaşi, 1999\n\n* Emanuel Swedenborg, **Despre înţelepciunea iubirii conjugale**. Versiune românească şi postfaţă, Editura Polirom, Iaşi\n\n* Sfîntul Augustin, **Confessiones / Confesiuni**. Ediţie bilingvă, traducere din limba latină, introducere şi note, Editura Nemira, Bucureşti, 2000\n\n* Jacob Grimm / Ernest Renan, **Două tratate despre originea limbajului**. Versiuni în limba română, cuvînt înainte, introduceri şi bibliografie, Editura Universităţii, Iaşi, 2001\n\n* Rivarol, **Discurs despre universalitatea limbii franceze**, urmat de **Maxime, reflecţii, anecdote, cuvinte de duh**. Antologie, traducere, introducere şi bibliografie, Institutul European, Iaşi, 2003\n\n* **Leviticul**. Traducere din limba greacă, introducere, note şi comentarii de Eugen Munteanu, în vol. Cristian Bădiliţă et alii (coord.), ***Septuaginta***, I, (Geneza, Exodul, Leviticul, Numerii, Deuteronomul), Colegiul Noua Europă/ Polirom, Bucureşti/ Iaşi, 2004\n\n* **Odele**. Traducere din limba greacă, introducere, note şi comentarii de Eugen Munteanu, în vol. Cristian Bădiliţă et alii (coord.), ***Septuaginta***, IV/I (Psalmii, Odele, Proverbele, Ecleziastul, Cântarea cântărilor), Polirom, Iaşi, 2005.\n\n* **Cartea înţelepciunii lui Iisus Sirah**. Traducere din limba greacă, introducere, note şi comentarii de Eugen Munteanu, în vol. Cristian Bădiliţă et alii (coord.), ***Septuaginta***, V (cărţile sapienţiale), Polirom, Iaşi (în curs de apariţie).\n\n* Rudolf Windisch, **Studii de lingvistică şi filologie românească**. Editori: Eugen Munteanu, Oana Panaite, Editura Universității „Alexandru Ioan Cuza”, Iași, 2006\n\n* Wilhelm von Humboldt, **Diversitatea structurală a limbilor şi influenţa ei asupra umanităţii**. Versiune românească, introducere, notă asupra traducerii, tabel cronologic, bibliografie şi indice. Editura Humanitas, Bucureşti, 2008\n\n* Eugeniu Coşeriu, **Istoria filosofiei limbajului. De la începuturi până la Rousseau**. Ediţie nouă, augmentată de Jörn Albrecht, cu o remarcă preliminară de Jürgen Trabant, versiune românească şi indice de Eugen Munteanu şi Mădălina Ungureanu, cu o prefaţă la ediţia românească de Eugen Munteanu, Editura Humanitas, Bucureşti, 2011\n\n* Gh. Ivănescu, **Problemele capitale ale vechii române literare**. Ediţia a II-a, revizuită, indici şi bibliografie de Eugen Munteanu şi Lucia-Gabriela Munteanu, cu o postfaţă de Eugen Munteanu, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2012\n\n#Lucrări de referință\n\n* H. Tiktin, **Rumänisch-deutsches Wörterbuch**, 2. überarbeitete und ergänzte Auflage, hrsg. von Paul Miron, Otto Harrassowitz Verlag, Wiesbaden, 3 vol., 1985-1987 (Premiul “Timotei Cipariu” al Academiei Române pe anul 1989).\n\n* **Biblia 1688**, Pars I, Genesis, Iaşi 1988, Pars II, Exodus, în seria ***Monumenta linguae Dacoromanorum***, Iaşi 1991 (premiul “Bogdan Petriceicu Hasdeu” al Academiei Române pe anul 1991).\n\n* **Biblia 1688**, Pars III, Leviticus, în seria ***Monumenta linguae Dacoromanorum*** (contribuţia personală: Comentarii lingvistice, filologice şi exegetice, p. 163-204, precum şi versiunea explicativă modernă, i.e. a cincea coloană a ediţiei).\n\n* **Enciclopedia Bibliei**, Editura Logos, Cluj, 1996, Capitolul *Sfînta Scriptură în limba română*, p. 75-83.\n\n* Academia Română, **Dicţionarul limbii române** (DLR), tomul XIII, partea I, litera V, V-veni, Editura Academiei Române, Bucureşti, 1997\n\n* Academia Română, **Dicţionarul limbii române** (DLR), tomul IV, partea I, litera L, L-lherzolită, Editura Academiei Române, Bucureşti, 2008\n\n* Academia Română, **Dicţionarul limbii române** (DLR), tomul V, litera L, Li –luzulă, Editura Academiei Române, Bucureşti, 2008.\n\n* **Biblia 1688**, Pars VII, Regum I-II, în seria ***Monumenta linguae Dacoromanorum***, Editura Universităţii “Alexandru Ioan Cuza”, Iaşi, 2008. Premiul de excelenţă la «Salonul cărţii româneşti», Iaşi, 5-9 mai 2009 (consultant ştiinţific).\n\n* **Biblia 1688**, Pars VIII, Paralipomenon I-II, în seria ***Monumenta linguae Dacoromanorum***, Editura Universităţii “Alexandru Ioan Cuza”, Iaşi, 2011 (coordonator al noii serii).\n\n# Volume omagiale, volume ale unor manifestări științifice\n\n* **Studia linguistica et philologica in honorem D. Irimia**, Iaşi, Editura Universităţii, 2004 (coeditor alături de Ana-Maria Minuţ)\n\n* **Signa in rebus. Studia linguistica et semiologica in honorem M. Carpov**, Iaşi, Editura Universităţii, 2005 (coeditor alături de Mihaela Lupu, Florin Olaru şi Iulian Popescu)\n\n* **Quaderni della Casa Romena di Venezia**, VII, 2010. Atti del Congresso Internazionale **La Tradizione biblica romena nel contesto europeo** (Venezia, 22-23 aprilie 2010), a cura di Eugen Munteanu, Ana-Maria Gînsac, Corina Gabriela Bădeliţă, Monica Joiţa, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2010\n\n* **Receptarea Sfintei Scripturi, între filologie, hermeneutică şi traductologie**, I, Lucrările Simpozionului Naţional „Explorări în tradiţia biblică românească şi Europeană”, Iaşi, 28-29 octombrie 2010. Editori: Eugen Munteanu (coord.), Ioan Florin Florescu, Ana-Maria Gînsac, Maria Moruz, Sabina Savu-Rotenştein, Mădălina Ungureanu, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2011\n\n* **Receptarea Sfintei Scripturi, între filologie, hermeneutică şi traductologie**, II, Lucrările Simpozionului Naţional „Explorări în tradiţia biblică românească şi Europeană”, Iaşi, ediţia a II-a, 4-5 noiembrie 2011. Editori: Eugen Munteanu (coord.), Ana-Maria Gînsac, Maria Moruz, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2012.\n\n# Articole (selectiv)\n\n* ***Innovationsmechanismen im aktuellen intellektuellen Wortschatz des Rumänischen***, în **Zeitschrift für romanische Philologie**, 125: 3 (2009), p. 495-529.\n\n* ***Das choronymische Mikrosystem des Rumänischen. Eine historische Darstellung***, în vol.: Wolfgang Ahrens, Sheila Embleton, André Lapierre (eds), **Names in Multi-Lingual, Multi-Cultural and Multi-Ethnic Contact**. Proceedings of the 23rd International Congress of Onomastic Sciences, August 17-22, 2008, York University, Toronto, Canada, Published by York University, Toronto, Canada, 2009, p. 740-750.\n\n* ***Le nid ethno-choronymique «juif» en roumain. Approche historique et systématique***, în Maria Iliescu, Heidi Siller-Runggaldier, Paul Danler (eds.), **Actes du XXVe Congrès International de Linguistique et de Philologie Romanes** (Innsbruck, 3-8 septembre 2007), tom. III, De Gruyter, 2010, p. 325-336.\n\n* ***Sulla tradizione biblica romena. Dissociazioni di principio***, în **Quaderni della Casa Romena di Venezia**, VII, 2010. „Atti del Congresso Internazionale «La Tradizione biblica romena nel contesto europeo»” (Venezia, 22-23 aprilie 2010), a cura di Eugen Munteanu, Ana-Maria Gînsac, Corina Gabriela Bădeliţă, Monica Joiţa, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2010, p. 15-26.\n \n","didactic":"* **Lingvistică generală**, anul I, studii de licență\n\n* **Istoria limbii române literare**, anul al III-lea, studii de licență\n\n* **Principii și tehnici ale analizei lingvistice**, anul I, studii de masterat\n\n* **Politici lingvistice în evoluția românei literare**, anul I, studii de masterat\n\n* **Semantică lexicală**, anul al II-lea, studii de masterat\n\n* **Proiectarea și administrarea cercetării filologice**, anul I, Școala Doctorală de Studii Filologice, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza” din Iași\n\n* **Filosofia limbajului**, anul I, Școala Doctorală de Studii Filologice, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza” din Iași\n","administrative":"Director executiv al Școlii Doctorale de Studii Filologice, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza” (2006-2009)\n\nDirector al **Institutului de Filologie Română „A. Philippide”**(2009-2013)\n\nPreședinte fondator al **Asociației de Filologie și Hermeneutică Biblică din România** (din 2010)\n\nMembru în Comisia de Filologie, Consiliul Național de Atestare a Titlurilor, Diplomelor și Certificatelor Universitare (CNATDCU)\n","photo":"/images/professors/eugen-munteanu.jpg","email":"eugen-munteanu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.772Z","updatedAt":"2015-10-04T12:31:02.439Z","id":"55ec67ddaae1a18a09031a15"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Mihaela Secrieru","username":"mihaela-secrieru","level":"Profesor universitar","status":"Cadrul didactic titular, **Catedra de Limbă română și Lingvistică generală**\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nChair al conferinţei **Statutul ştiinţific al limbii române în universităţile străine – catalizator al lingvisticii şi literaturii româneşti**, sub tema **Diaspora în cercetarea ştiinţifică românească şi învăţământul superior**, Bucureşti, UEFISCDI ediţia a II-a, 25-27 septembrie 2012\n\nDirector al simpozionului ştiinţific studenţesc **Formarea iniţială a profesorului student în didactica specialităţii**\n\nEditor al revistei ştiinţifice studenţeşti **Iniţieri didactice**, Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, ISSN 1841-540","scientific":"# Volume\n\n* **Bibliografie signaletică de didactică a limbii şi literaturii române: (1757-2010)**, Editura PIM, Iași, 3 volume, 2011\n\n* **Didactica limbii române în contextul european al începutului de secol XXI**, Editura Universitas XXI, Iaşi, 2009\n\n* **Didactica limbii române**, Editura Ovi-Art, Botoşani, 2004, reeditată și adăugită în ediții succesive\n\n* **Bibliografie signaletică de didactică a limbii şi literaturii române**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2007\n\n* **Cumulul de funcţii sintactice în limba română** (“elementul predicativ suplimentar”), Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, 2001\n\n* **Elemente de sintaxă sincronică comparată**, Editura Universitas XXI, Iaşi, 2000\n\n* **Nivelul sintactic al limbii române**, Editura Geea, Botoşani, 1998\n\n#Volume colective\n\n* Mihaela Secrieru, Octavian Gordon, Emilia Parpală (coord.), **Statutul ştiinţific al limbi române în universităţile străine – catalizator al lingvisticii şi literaturii româneşti**, Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, 2012\n\n* Mihaela Secrieru, Simona Moraru, Roxana-Doina Murarasu, **Studii de didactică a limbilor moderne**, Editura Universitas XXI, Iaşi, 2008\n\n* Mihaela Secrieru (prim autor, coord.), Mihaela Doniga, **Elemente de surdo-didactica limbii şi literaturii române pentru gimnaziu**, Editura Universitas XXI, Iaşi, 2005\n\n#Articole (selectiv)\n* ***Lo status attuale della lingua romena nella Republica di Moldavia***, în volumul **Comunicare. Identitate. Comparatism**, Lucrările conferinţei internaţionale CIC2012, Craiova, 2-3 noiembrie, 2012, coord. Emilia Parpală, Editura Universitaria, Craiova, 2013, p. 341-354\n\n* ***O nouă încercare de taxinomie a acordului gramatical***, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi**, secţiunea III e, **Lingvistică**, tomul LX, 2014, p. 311-321.\n\n* ***Note privind conceptul de acord sintactic***, în Emilia Parpală (editor), **Identităţi discursive. O abordare comparativă şi comunicaţională**, Craiova, Universitaria, 2014, p. 182-191.\n\n* ***The Grammar of Romanian***, Edited by Gabriela Pană Dindelegan, Consultant Editor Martin Maiden, Oxford University Press, 2013, 656 p., în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi**, secţiunea III e, **Lingvistică**, tomul LX, 2014, p. 373-378\n\n* ***Viorica Popa, Fenomenul analogiei în limbă, Editura Fundaţiei Academice Axis, Iaşi, 2012, 199 p.***, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi**, secţiunea III e, **Lingvistică**, tomul LX, 2014, p. 378-381\n\n* ***Statutul actual al limbii române în Republica Moldova***, în volumul **Diversitatea lingvistică şi dialogul intercultural în procesul de comunicare**, volum constituit pe baza lucrărilor prezentate în cadrul Conferinţei ştiinţifice cu participare internaţională „Diversitatea lingvistică şi dialogul intercultural în procesul de comunicare”, Chişinău, 28-30 martie, 2013, volumul (2), Coordonatori: Ludmila Branişte şi Gina Nimigean, Editura Vasiliana 98, Editura Garomont-Studio, Iaşi – Chişinău, 2013, p. 149-173\n\n* ***Lo status attuale della lingua romena nella Republica di Moldavia***, în volumul **Comunicare. Identitate. Comparatism**, Lucrările conferinţei internaţionale CIC2012, Craiova, 2-3 noiembrie, 2012, coord. Emilia Parpală, Editura Universitaria, Craiova, 2013, p. 341-354.\n\n* ***Contribuţia lui G. Ivănescu la dezvoltarea sintaxei româneşti***, în **Anuar de lingvistică şi istorie literară** [ALIR], T. LIII, 2013, Lucrările Colocviului internaţional „G. Ivănescu-100 de ani de la naştere”, Filiala Academiei Române din Iaşi, Editura Academiei Române, 2014, p. 349-362\n\n* ***Moldawien. Die Sprachenpolitik der Republik Moldau aus der Perspektive der ECRM***, **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi** (serie nouă), Secţiunea III e **Lingvistică**, Tomul. LVIII/2012, p. 223-238\n* ***Raport ştiinţific al WE Statutul ştiinţific al limbii române în universităţile străine – catalizator al lingvisticii şi literaturii româneşti***, în volumul conferinţei **Diaspora în cercetarea ştiinţifică românească şi învăţământul superior „Seminţe de viitor”**, Bucureşti, UEFISCDI ediţia 2012, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2012, p. 256- 269\n\n* ***Scientific Report on the Exploratory Workshop The scientific status of Romanian in foreign Universities - A Catalyst of Romanian Linguistics and Literature***, in **Papers of International Conference Diaspora in Romanian scientific Research and Higher Education „Seeds for the Future”**, 25-28 September, Bucharest, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2012, p. 270 - 278.\n\n* ***Meda Gabriela Gautschi, Studio contrastivo degli usi del futuro in italiano e romeno – lingua scritta e lingua parlata a confronto, Editura Echinox, 2010, 207 pag.***, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi** (serie nouă), Secţiunea III e **Lingvistică**, Tomul. LVII/2011, p. 179-180\n\n* ***Laurence Benetti: L’article zéro en français contemporain. Aspects syntaxiques et sémantiques. Bern: Lang 2007, VII+187S. (Publications Universitaires Européenes, 307)***, în **Romanische Forschungen**, nr. 122 (2010), Frankfurt am Main, 2010, p. 111-113.\n\n* ***Republic of Moldavia – an intermezzo on the signing and the ratification of the European Charter for Regional and Minority Languages***, în **Philologica Jassyensia**, an VI, Nr. 2 (12), 2010, p. 231-244\n\n* Interviu cu profesorul Roger Morger, Revista **Iniţieri didactice**, nr. 45-6/2009-2010, p. 5-13.","didactic":"* **Limba română: Sintaxă**, anul al II-lea, studii de licență\n\n* **Istoria sintaxei româneşti**, anul I, studii de masterat\n\n* **Semiotică şi comunicare**, anul I, studii de masterat\n","administrative":"Formator CNFP între anii 2006-2010\n\nMembră Comisia 3 CNCSIS Romania şi expert evaluator între anii 2008-2011\n\nExpert evaluator internaţional pentru BNSF - Bulgaria în anul acad. 2009-2010\n\nExpert evaluator al ARACIS, cf. RNE, din 2010+\n\nMembră în Consiliul profesoral al Facultăţii de Litere în anul universitar 2007-2008\n\nMembră în Consiliul Catedrei de Limbă română în anul universitar 2007-2008\n","photo":"/images/professors/mihaela-secrieru.jpg","email":"mihaela-secrieru@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.781Z","updatedAt":"2015-10-04T08:54:58.307Z","id":"55ec67ddaae1a18a09031a16"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Luminița Hoarță Cărăușu","username":"luminita-hoarta-carausu","level":"Profesor universitar","status":"Cadru didactic titular, **Catedra de Limbă română și Lingvistică generală**\n\nConducător de lucrări de doctorat\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nReferent ştiinţific al **Editurii Universitare**, Bucureşti\n\nEditor-coordonator al revistei ştiinţifice ***Caiete ieşene***, Editura Cermi, Iaşi\n\nMembru al Colegiului de redacţie al revistei ***Philologos***, Editura Sedcom Libris, Iaşi\n\nMembru în Colegiul de redacţie al revistei [***Speech and Context. International Journal of Linguistics, Semiotics and Literary Science***][1], redactor-şef Angelica Coşciug, tipografia Universităţii de stat „Alecu Russo” din Bălţi, Republica Moldova\n\nReferent ştiinţific al revistei [***Limbă şi Literatură. Repere identitare în context european***][2], Facultatea de Litere, Universitatea din Piteşti\n\n  [1]: http://speech-and-context.blogspot.ro/\n  [2]: http://www.upit.ro/uploads/facultatea_lit/ELI/editorial%20board%20site%20ELI.pdf","scientific":"#Volume\n\n* **Dinamica morfosintaxei şi pragmaticii limbii române actuale**, ediția a doua, Editura Cermi, Iaşi, 2008\n\n* **Teorii şi practici ale comunicării**, Editura Cermi, Iaşi, 2008\n\n* **Dinamica morfosintaxei şi pragmaticii limbii române actuale**, Editura Cermi,  Iaşi, 2007\n\n* **Pragmalingvistică. Concepte şi taxinomii**, Editura Cermi, Iaşi, 2004\n\n* **Elemente de analiză a structurii conversaţiei**, Editura Cermi, Iaşi, 2003\n\n* **Probleme de morfologie a limbii române**, Editura Cermi, Iaşi, 2001\n\n* **Probleme de sintaxă a limbii române**, Editura Cermi, Iaşi, 1999\n\n\n#Ediții și corpusuri\n\n* **Corpus de limbă română vorbită actuală nedialectală**, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2013, (coordonator)\n\n* **Limba română vorbită actuală. Studiu lingvistic aplicat**, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2012, (coordonator)\n\n* **Pragmatica pronumelui**, Editura Cermi, Iaşi, 2005 (coordonator)\n\n* **Corpus de limbă română vorbită actuală**, Editura Cermi, Iaşi, 2005, (coordonator)\n\n#Volume ale unor manifestări științifice\n\n* **Spaţiul lingvistic şi literar românesc în orizont european**, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, Iaşi, 2009, (coordonatori: Luminiţa Hoarţă Cărăuşu şi Lăcrămioara Petrescu).\n\n* **Comunicarea. Ipoteze şi ipostaze**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2008, (coordonator)\n\n* **Rezultate şi perspective actuale ale lingvisticii româneşti şi străine**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2007, (coordonator)\n\n#Articole (selectiv)\n\n* ***Dezbaterea politică televizată. Studiu de caz***, în revista **Speech and Context International Journal of Linguistics, Semiotics and Literary Science**, Universitatea de stat „Alecu Russo”, Bălţi, Republica Moldova, Anul III, nr. 1, (V), 2013, p. 95-105\n\n* ***Dezbaterea televizată românească actuală. Personalităţi ale diasporei***, în **Cultură şi identitate românească. Tendinţe actuale şi reflectarea lor în diaspora**, volum coordonat de Ofelia Ichim, *Actele Simpozionului internaţional Cultură şi identitate românească. Tendinţe actuale şi reflectarea lor în diaspora* organizat de Institutul de Filologie Română „A. Philippide” al Academiei Române, Filiala Iaşi, în colaborare cu Asociaţia Culturală „A. Philippide” (Iaşi, 22-24 septembrie 2010), Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2013, p. 423-432\n\n* ***Interacţiunea mediată controlată. Comportamentul comunicativ al interlocutorilor în talk show-ul românesc actual (invitaţi – români din diasporă)***, în **Diaspora culturală românească: paradigme lingvistice, culturale şi etnofolclorice**, *Actele Simpozionului internaţional Diaspora culturală românească: paradigme lingvistice, culturale şi etnofolclorice*, organizat de Institutul de Filologie Română „A. Philippide” al Academiei Române, Filiala Iaşi, în colaborare cu Asociaţia Culturală „A. Philippide” (Iaşi, 5-7 noiembrie 2009), Editura Alfa, 2013, p. 39-59\n\n* ***Interacţiunea verbală de tip conflictual. Particularităţi pragmatice***, în volumul Simpozionului Naţional **Limbajul medical – istoric şi perspective** – (Updatarea limbajului medical românesc la standardele europene), , Ediţia a XIII-a, 23 martie 2013, Universitatea de Medicină şi Farmacie „Grigore T. Popa”, Editura „Gr. T. Popa” Iaşi, 2013, p. 140-153\n\n* ***Ein Aspekt der religiösen Textmorphologie aus dem 16. Jahrhundert: das Zeitwort (analytische Strukturen auf der Flexionsebene***, în volumul Conferinţei **Text şi discurs religios**, Iaşi, 10-12 noiembrie 2011, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2012, p. 131-137.\n\n* ***Aspecte gramaticale ale textelor religioase din secolul al XVI-lea***, în volumul Simpozionului naţional (cu participare internaţionala organizat de Universitatea de Medicină şi Farmacie „Grigore T. Popa”, Iaşi, Facultatea de Medicină, Disciplina Limbi moderne, Iaşi 26 mai 2012, **Limbajul medical. Istoric şi perspective**, Editura Alfa, Iaşi, 2012, p. 129-140.\n\n* ***Aspecte ale problematicii construcţiei pasive***, în **Analele ştiinţifice ale Universităţii „Dunărea de Jos”, Galaţi**, Fascicula XXIV, Anul V, nr. 1-(7), 2012, *Lexic comun/Lexic specializat*, Editura Europlus, 2012, p. 155-163\n\n* ***Un aspect al morfologiei textelor religioase din secolul al XVI-lea: verbul (structuri analitice la nivel de flexiune)****, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi**, Secţiunea IIIe **Lingvistică**, Tomul LVIII/2012, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2012, p. 27-32.\n\n* ***Talk show-ul românesc actual. Elemente de pragmatică lingvistică aplicată***, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi**, Secţiunea IIIe **Lingvistică**, Tomul LV/2009, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2012, p. 65-89.\n\n* ***Un tip de interacţiune mediată controlată. Dezbaterea televizată românească actuală***, în revista **Limbaj şi context. Revistă de lingvistică, semiotică şi ştiinţă literară**, Universitatea de stat „Alecu Russo”, Bălţi, Republica Moldova, Anul III, nr. 1, 2011, p. 56-63\n\n* ***Ipostaze ale „discursului repetat” în discursul publicistic românesc actual***, în **Analele Universităţii „Dunărea de Jos” din Galaţi**, Fascicula XXIV, Anul IV, Nr. 2 (6), 2011, *Lexic comun/Lexic specializat, Actele Conferinţei internaţionale Lexic comun/Lexic specializat*, Ediţia a IV- a, 15-16 septembrie 2011, Editura Europlus 2011, p. 366-378\n\n* ***Un aspect al morfosintaxei textelor religioase din secolul al XVI-lea: predicatul complex cu operator modal şi aspectual***, în volumul ce conţine Lucrările conferinţei **Text şi discurs religios**, Iaşi, 12-13 noiembrie 2010, Ediţia a III-a, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2011, p. 95-100.\n\n* ***Interacţiunea mediată controlată. Dezbaterea televizată românească actuală***, în Mariana Flaişer (coord.), **Limbajul medical. Istoric şi perspective**, Ediţia a XI-a, Lucrările Simpozionului Naţional (cu participare internaţională) organizat de Disciplina de Limbi moderne a Departamentului „Ştiinţe interdisciplinare” al Universităţii de Medicină şi Farmacie „Gr. T. Popa”, Editura Alfa, Iaşi, 2011, p. 261-274.\n\n* ***Retorică şi pragmatică publicistică. „Figuri de construcţie” şi strategii persuasive în titlurile de articole din presa românească actuală***, în revista **Limbaj şi context. Revistă de lingvistică, semiotică şi ştiinţă literară**, Universitatea de stat „Alecu Russo”, Bălţi, Republica Moldova, Anul II, nr. 2, 2010, p. 23-32\n\n* ***Particularităţile retorico-pragmatice ale dezbaterii electorale televizate româneşti actuale***, în volumul Simpozionului Naţional Criza valorilor şi valorile crizei, organizat de Disciplina „Limbi moderne” a Departamentului „Ştiinţe interdisciplinare” al Universităţii de Medicină şi Farmacie „Gr. T. Popa”, Iaşi, Ediţia a X-a, Editura Alfa, Iaşi, 2010, p. 27-37.\n\n* ***Negocierea rolurilor comunicative în româna vorbită actuală***, în revista **Limbaj şi context. Revistă de lingvistică, semiotică şi ştiinţă literară**, Universitatea de stat „Alecu Russo”, Bălţi, Republica Moldova, nr. 1, 2009, p. 44-55\n\n* ***Semnificaţiile limbajului gestual***, în revista **Limbaj şi context. Revistă de lingvistică, semiotică şi ştiinţă literară**, Universitatea de stat „Alecu Russo”, Bălţi, Republica Moldova, nr. 2, 2009, p. 71-78\n\n* ***Un aspect al morfosintaxei textelor religioase din secolul al XVI-lea: construcţia pasivă***, în volumul ce conţine lucrările Conferinţei Naţionale **Text şi discurs religios**, Iaşi, 13-14 noiembrie 2009, Ediţia a II-a, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2010, p. 269-278.\n\n* ***Comunicarea ostensiv-inferenţială şi principiul pertinenţei în discursul politic românesc actual***, în volumul Colocviului internaţional de Ştiinţe ale Limbajului „Eugeniu Coşeriu”, ediţia a X-a, Suceava, **Limbaje şi comunicare. Creativitate, semnaticitate, alteritate**, Casa Editorială Demiurg, Iaşi, 2009, p. 296-307.\n\n* ***Distorsionări verbale şi nonverbale în exprimarea acordului şi dezacordului în româna vorbită actuală***, în volumul **Distorsionări în comunicarea lingvistică, literară şi etnofolclorică românească şi contextul european**, textele comunicărilor prezentate la simpozionul internaţional cu acelaşi nume (25-27 septembrie 2008, organizat de Academia Română filiala Iaşi, Institutul de Filologie Română „A. Philippide” şi Asociaţia culturală „A. Philippide”), Editura Alfa, Iaşi, 2009, p. 179-190.\n\n* ***Semnalele de receptare şi funcţiile lor pragmatice***, în **Spaţiul lingvistic şi literar românesc în orizont european** (coordonatori Luminiţa Hoarţă Cărăuşu şi Lăcrămioara Petrescu), Editura Universităţii „Alexandru Ioan Cuza” Iaşi, Iaşi, 2009, p. 164-177\n\n* ***Ipostaze publicistice ale „discursului repetat”***, în **Mélanges francophones**, nr. 4 (volum III,2), Actes de la conférence annuelle à l’occasion des Journées de la Francophonie, VIème édition, 27-29 mars 2009. Formes textuelles de la communication. De la production à la reception, p. 165-175, Galaţi: GUP.\n","didactic":"* **Limba română: Morfologie**, anul al II-lea, studii de licență\n\n* **Limba română: Sintaxă**, anul al II-lea, studii de licență\n\n* **Dinamica morfosintaxei**, anul I, studii de masterat\n\n* **Teorii și practici ale comunicării**, anul I, studii de masterat\n\n* **Pragmatică**, anul al II-lea, studii de masterat\n\n* **Doctrine lingvistice moderne**, curs modular, anul I, Școala Doctorală de Studii Filologice, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza” din Iași\n","administrative":"Membru al Comisiei pentru Certificarea naţională a competenţelor lingvistice, **Institutul limbii române**, Ministerul Educaţiei şi Cercetării\n\nMembru al **Autorităţii Naţionale pentru Cercetare Ştiinţifică – Unitatea Executivă pentru Finanţarea Învăţământului Superior a Cercetării Dezvoltării şi Inovării** (UEFISCDI) – membru (Expert/Evaluator) în Grupul Experţilor constituit pentru evaluarea propunerilor de Programe/Proiecte\n","photo":"/images/professors/luminita-hoarta-carausu.jpg","email":"luminita-hoarta-carausu@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.786Z","updatedAt":"2015-10-04T09:26:56.940Z","id":"55ec67ddaae1a18a09031a18"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Ana-Maria Minuț","username":"ana-maria-minut","level":"Conferenţiar universitar","status":"Cadru didactic titular, **Catedra de Limbă română și Lingvistică generală**\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\nCoordonator al colecției [***Logos**][1], Editura Universității „Alexandru Ioan Cuza”\n\nRedactor-șef al revistei ***Analele Științifice ale Universității „Alexandru Ioan Cuza” din Iași***, *secţiunea III e*, **Lingvistică**\n\n\n  [1]: http://www.editura.uaic.ro/colectii.php?colectie=logos","scientific":"#Volume\n* **Morfosintaxa verbului în limba română veche**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2002\n\n* **Formarea cuvintelor. Teorie şi practică**,  Editura SEDCOM LIBRIS, Iaşi, 2005 (co-autor: Petru Zugun)\n\n \n# Volume colective\n\n* **Limba română vorbită în Moldova istorică**, volumul II, **Texte**, Leipziger Universitätsverlag, Leipzig, 2000 (editori: Klaus Bochmann, Vasile Dumbravă)\n\n* **Corpus de limbă română vorbită actuală**, Editura Tehnică Ştiinţifică şi Didactică CERMI, Iaşi, 2005 (coordonator: Luminiţa Hoarţă Cărăuşu)\n \n#Volume ale unor manifestări științifice\n\n* **Limba română azi** (ediţia a X-a, Iaşi-Chişinău, 3-7 noiembrie 2006), Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2007 (co-editori: Dumitru Irimia, Ioan Milică)\n \n# Volume omagiale\n\n* **Studia linguistica et philologica in honorem D. Irimia**, tomul XLIX-L al Analelor ştiinţifice ale Universităţii „Alexandru Ioan Cuza”, secţiunea III e, Lingvistică, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2003­2004, 586 p. (co-editor: Eugen Munteanu)\n \n# Articole (selectiv)\n\n* ***Câteva note morfologice pe baza ms. 45***, în volumul **Al. Andriescu - 88**, editor: Gabriela Haja, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2014, p. 233-242\n\n* ***Der lateinische Grundbestand des Rumänischen. Die Romanisierung und die Entwicklung eines neuen linguistischen Systems***, în  Alexander Rubel (Hg., unter Mitwirkung von Iulia Dumitrache), **Imperium und Romanisierung. Neue Forschungsansätze aus Ost und west zu Ausübung, Transformation und Akzeptanz von Herrschaft im Römischen Reich**, Konstanz, Hartung-Gorre Verlag, 2013, (co-autor: Luminița Botoșineanu)\n\n* ***Observaţii cu privire la substantivele abstracte, masive, colective şi relaţionale din ms. 45***, în volumul **Omagiu lui C. Dimitriu la 80 de ani**, coordonatori: Aurelia Merlan, Rodica Nagy, Editura Universităţii „Ştefan cel Mare”, Suceava, 2013, p. 387-402\n\n* ***Ion Budai-Deleanu, Opere: Ţiganiada, Trei viteji, Scrieri lingvistice, Scrieri istorice, Traduceri. Ediţie îngrijită, cronologie, note şi comentarii, glosar şi repere critice de Gheorghe Chivu şi Eugen Pavel. Studiu introductiv de Eugen Simion, Bucureşti, Academia Română, Fundaţia Naţională pentru Ştiinţă şi Artă, Colecţia „Opere fundamentale”, 2011, 1354 p., ISBN 978-606-555-071-1***, în **Philologica Jassyensia**, nr. 1 (17), 2013, p. 269-272\n\n* ***Sens şi consens în creaţia lexicală. Analiza motivaţională a denumirilor unor soiuri de fructe şi legume (după NALR. Moldova şi Bucovina, vol. al III-lea)***, în volumul In **Magistri Honorem Vasile Frăţilă. 50 de ani de carieră universitară**, coordonator: Ana-Maria Pop, Editura Ardealul, Târgu-Mureş, 2012, p. 117-139 (co-autor: Luminița Botoșineanu)\n\n* ***Fondul latin al românei. Romanizarea şi apariţia unui nou sistem lingvistic***, în volumul simpozionului internațional Romanisierung. Theorie und Praxis eines Forschungskonzepts (Internationale Tagung im Rahmen der Universitätspartnerschaft Konstanz-Iasi), **Romanizarea. Impunere şi adeziune în Imperiul Roman**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2011, p. 245-254 (co-autor: Luminița Botoșineanu)\n","didactic":"* **Limba română: Lexicologie**, anul I, studii de licență\n\n* **Dinamica lexicului**, anul I, studii de masterat\n\n* **Doctrine lingvistice moderne**, curs modular, anul I, Școala Doctorală de Studii Filologice, Facultatea de Litere, Universitatea „Alexandru Ioan Cuza” din Iași\n","administrative":"Administrativ [text aici...]","photo":"/images/professors/ana-maria-minut.jpg","email":"ana-maria-minut@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.855Z","updatedAt":"2015-10-04T10:02:12.081Z","id":"55ec67ddaae1a18a09031a1a"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Sorin Guia","username":"sorin-guia","level":"Conferenţiar universitar","status":"Cadru didactic titular","scientific":"# Volume\n* **Discursul religios. Structuri şi tipuri**, Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2014\n\n* **Elemente de dialectologie română**, Editura Vasiliana 98, Iași, 2014\n\n* **Dialectologie română. Studii și corpus de texte**, Editura Vasiliana 98, Iași, 2014\n\n#Ediții\n\n* **Palia de la Orăştie (1582)**, I, **Textul**, Editura Universităţii „Alexandru Ioan Cuza” , Iaşi, 2005 (autori: Vasile Arvinte, Ioan Caproşu, Alexandru Gafton, Sorin Guia)\n\n* **Palia de la Orăştie (1582)**, II, **Studii** şi **Indice de cuvinte**, Editura Universităţii „Alexandru Ioan Cuza” Iaşi, 2007 (autori: Vasile Arvinte, Ioan Caproşu, Alexandru Gafton, Sorin Guia)\n \n#Volume colective\n\n* **Corpus de limbă română vorbită actuală**, Editura tehnică, ştiinţifică şi didactică Cermi, Iaşi, 2005 (coord: Luminița Hoarță Cărăușu)\n \n#Articole (selectiv)\n\n* ***Arguments based on authority in the religios discours***, în **Studies on literature, discourse and multicultural dialogue** (coord. Iulian Boldea), Editura Arhipelag XXI, Tg. Mureş, 2013, p. 501-510\n\n* ***Graiul din comuna Gîrda de Sus, judeţul Alba***, în **Analele Ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi***, secţiunea III e, **Lingvistică**, tomul LIX / 2013, p. 145-156\n\n* ***Structures argumentatives dans le discourse religieux de Bucovine***, în **Text şi discurs religios**, nr. IV (editori: Alexandru Gafton, Sorin Guia, Ioan Milică), Editura Universităţii „Alexandru Ioan Cuza”, Iaşi, 2012, p. 307- 317\n\n* ***Aspecte fonetice şi lexicale în graiul din comuna Mănăstirea Humorului, judeţul Suceava***, în **Analele ştiinţifice ale Universităţii „Alexandru Ioan Cuza” din Iaşi** (serie nouă), secţiunea III e, **Lingvistică**, tomul LVIII / 2012, p. 179-188\n\n* ***Aspecte dialectale în graiul din comuna Scărişoara, judeţul Alba***, în **Proceedings of the International Conference “Communication, context, interdisciplinarity”**, Editura Universității „Petru Maior”, Târgu-Mureș, 2012, p. 1074-1078\n\n* ***The religious discourse generated by the titular saint of a church*** în **Langue et litttérature. Repères identitaires en contexte européen** (Lucrările celei de-a IX-a Conferinţe Internaţionale a Facultăţii de Litere, Piteşti, 17-19 iunie 2011), nr. 7/2011, Editura Universităţii din Piteşti, 2011, p. 107-114\n\n* ***Aspecte ale graiului din comuna Vicovu de Jos***, judeţul Suceava, în **Limba română: ipostaze ale variaţiei lingvistice**. Actele celui de-al 10-lea colocviu al catedrei de limba română, Bucureşti, 3-4 decembrie 2010, editori: Rodica Zafiu, Camelia Uşurelu, Helga Bogdan Oprea, Editura Universităţii din Bucureşti, 2011, p. 423-431\n\n* ***Lʼ état actuel de la palatalisation des labiales dans le sous-dialecte moldave (étude basée sur les donées figurant dans le NALR. Moldavie et Bucovine)***, în **Langue et litttérature. Repères identitaires en contexte européen** (Lucrările celei de-a VIII-a Conferinţe Internaţionale a Facultăţii de Litere, Piteşti, 4-6 iunie 2010), nr. 6/2010, Editura Universităţii din Piteşti, 2010, p. 55-64\n\n* ***Norm and variation in romanian dialects***, în **Perspectives contemporaines sur le monde médiéval/ Contemporary perspectives on the medieval world**, 3-5 decembrie 2010, Piteşti, România, Nr. 2/2010, Redactor şef: Laura Bădescu, Editura Tiparg, Pitesti, 2010, p. 140-144\n\n* ***Structuri argumentative în discursul religios. Cuvîntarea religioasă prilejuită de sfinţirea bisericii***, în **Limba română: controverse, delimitări, noi ipoteze**, II, *Pragmatică şi stilistică* (Actele celui de-al 9-lea colocviu al catedrei de limba română, Bucureşti, 4-5 decembrie 2009), editori: Rodica Zafiu, Adina Dragomirescu, Alexandru Nicolae, Editura Universităţii din Bucureşti, 2010, p. 79-85","didactic":"* **Limba română: Fonetică și dialectologie**, anul I, studii de licență\n\n* **Lingvistică generală și aplicată**, anul al II-lea, studii de masterat\n","administrative":"Fondator și președinte al **Asociației Culturale „Text și discurs religios”**\n\nMembru în **Consiliul profesoral al Facultății de Litere**, Universitatea „Alexandru Ioan Cuza” (din 2012)\n","photo":"/images/professors/sorin-guia.jpg","email":"sorin-guia@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.784Z","updatedAt":"2015-10-04T10:27:38.259Z","contact":"#E-mail\nsoringuia01@yahoo.com","id":"55ec67ddaae1a18a09031a17"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Roxana Vieru","username":"roxana-vieru","level":"Lector universitar","status":"Cadru didactic titular, **Catedra de Limbă română și Lingvistică generală**\n\nConducător de lucrări metodico-științifice pentru obținerea gradului didactic I\n\nConducător de disertații\n\nConducător de lucrări de licență\n\n","scientific":"#Volume\n\n* **Studiu lingvistic asupra Paliei de la Orăștie**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2014\n\n#Ediții\n\n* Alexandru Philippide, **Originea românilor**, I, prefață de  Carmen Pamfil, Editura Universității „Alexandru Ioan Cuza”, Iași, 2014\n\n* Alexandru Philippide, **Originea românilor**, II, Editura Universității „Alexandru Ioan Cuza”, Iași, 2015 (sub tipar)\n\n#Volume colective\n\n* **Corpus de limbă română vorbită actuală nedialectală**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2013 (coord: Luminița Hoarță Cărăușu)\n\n* **Limba română vorbită actuală. Studiu lingvistic aplicat**, Editura Universității „Alexandru Ioan Cuza”, Iași, 2012 (coord: Luminița Hoarță Cărăușu)\n\n* **Corpus de limbă română vorbită actuală**, Editura Tehnică, Ştiinţifică şi Didactică CERMI, Iaşi, 2005 (coord: Luminița Hoarță Cărăușu)\n\n#Articole \n* ***Motivated Signs in the Denomination of Musical Instruments***, în **Globalization and Intercultural Dialogue. Multidisciplinary Perspectives, Language and Discourse Section**, Arhipelag XXI Press, Târgu-Mureș, 2014, p. 294-300\n\n* ***Arme medievale – abordare lexico-semantică***, în **The Proceedings of the European Integration – between Tradition and Modernity Congress**, Editura Universității „Petru Maior”, vol. 5, 2013, p. 495-509\n\n* ***Fenomene fonetice în Evanghelia (1697) lui Antim Ivireanul***, în **The Proceedings of the European Integration – between Tradition and Modernity Congress**, Editura Universității „Petru Maior”, vol. 5, 2013, p. 373-381\n\n* ***Originea Romînilor – din nou sub tipar***, în **Analele ştiinţifice ale Universităţii «Alexandru Ioan Cuza» din Iaşi**, Secţiunea IIIe **Lingvistică**, Tomul LIX/2013, In Memoriam ALEXANDRU PHILIPPIDE, Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, p. 75-78\n\n* ***Some Elements of Hungarian Origin in Noul Testament de la Bălgrad (1648)***, în **Text şi discurs religios**, editori: Alexandru Gafton, Sorin Guia, Ioan Milică, Lucrările Conferinţei Naţionale „Text şi discurs religios”, Iaşi, 9-10 noiembrie 2012, Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, 2013, p. 163-172\n\n* ***Slavic Lexical Elements in Antim Ivireanu’s Evanghelia (1697)***, în **Text şi discurs religios**, editori: Alexandru Gafton, Sorin Guia, Ioan Milică, Lucrările Conferinţei Naţionale „Text şi discurs religios”, Iaşi, 10-12 noiembrie 2011, Editura Universităţii „Alexandru Ioan Cuza” din Iaşi, 2012, p. 81-89\n\n* ***Lexical Characteristics of Coresi’s Text Apostolul***, în **Limba şi Literatura. Repere identitare în context european. – Langue et Littérature. Repères identitaires en contexte européen – Language and Literature. European Landmarks of Identity**, Lucrările celei de-a VIII-a Conferinţe Internaţionale a Facultăţii de Litere, Piteşti, 17-19 iunie 2011, Editura Universității din Pitești, p. 148-155\n\n* ***Palia de la Orăştie, The Reflection of the Sources in the Romanian Translation***, în **Limba şi Literatura. Repere identitare în context european. – Langue et Littérature. Repères identitaires en contexte européen – Language and Literature. European Landmarks of Identity**, Lucrările celei de-a VIII-a Conferinţe Internaţionale a Facultăţii de Litere, Piteşti, 4-6 iunie 2010, Editura Universităţii din Piteşti, 2010, p. 128-133","didactic":"* **Limba română: Morfologie**, anul al II-lea, studii de licență\n\n* **Istoria limbii române**, anul al II-lea, studii de licență\n\n* **Paleografie româno-chirilică**, anul I, studii de masterat\n\n* **Teorii și practici ale comunicării**, anul I, studii de masterat\n\n* **Dinamica morfosintaxei**, anul I, studii de masterat\n\n* **Pragmatica**, anul II, studii de masterat\n\n* **Onomasiologie**, anul II, studii de masterat\n","administrative":"Membru în comisia de realizare a orarului Facultății de Litere\n\n","photo":"/images/professors/roxana-vieru.jpg","email":"roxana-vieru@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.873Z","updatedAt":"2015-10-04T13:12:18.086Z","id":"55ec67ddaae1a18a09031a1c"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limba română și Lingvistică generală","hidden":false,"createdAt":"2015-09-06T16:20:44.239Z","updatedAt":"2015-09-06T16:20:45.085Z","id":"55ec67dcaae1a18a090319f6"},"name":"Simona Ailenii","username":"simona-ailenii","level":"Asistent universitar","status":"Cadru didactic titular, **Catedra de Limbă română și Lingvistică generală**\n\nConducător de lucrări de licență\n","scientific":"# Capitole în volume colective\n* ***A onomástica tristaniana. O caso Brunor le Noir***, **Aproximaciones y revisiones medievales: historia, lengua y literatura**, eds. Concepción Company, Aurelio González și Lillian von der Walde Moheno, El Colegio de México: Universidad Nacional Autónoma de México/Universidad Autónoma Metropolitana, México, D. F., 2013, pp. 445-459.\n\n* ***Livro de Merlin galicien-portugais. Quelques aspects de collatio et traduction***, **Temps et mémoire dans la littérature arthurienne: actes du Colloque International de la Branche Roumaine de la Société Internationale Arthurienne**, Bucarest, 14-15 mai 2010, Voicu, Mihaela; Gârbea, Cătălina; Popescu, Andreea (editori), Editura Universității din București, București, 2011, pp. 346-356.\n\n* ***O fragmento galego-português do Livro de Tristan. Aspectos de collatio e tradução***, **Actas del XIII Congreso Internacional Asociación Hispánica de Literatura Medieval (Valladolid, 15 a 19 de septiembre de 2009), In memoriam Alan Deyermond**, vol. I, José Manuel Fradejas Rueda, Déborah Dietrick Smithbauer, Demetrio Martín Sanz, Mª Jesús Díez Garretas (editori), Asociación Hispánica de Literatura Medieval, Valladolid, 2010. pp. 287-297.\n\n* ***A morte de Galaad e Perceval***, **Caballerías**, Lillian von der Walde M.; Mariel Reinoso I. (editori), Diciembre 2009 – Enero 2010, Año 4, Número 23, México, 2010, pp. 232-256.\n\n#Volume ale unor manifestări științifice\n* Miranda, José Carlos Ribeiro; Laranjinha, Ana Sofia; Correia, Isabel; Ailenii, Simona (editori), **Estória do Santo Graal (Livro Português de José de Arimateia)**, ediția manuscrisului 643 de la Arquivo Nacional da Torre do Tombo, 2 vols., Estratégias Criativas, Porto, 2013.\n\n* Petrovici, Oana Maria; Ailenii, Simona; Nimigean, Gina; Braniște, Ludmila; Munteanu, Vladina; Leon, Laura Ioana; Polozova, Arina; Băiceanu, Lucian; Galușcă, Lilia (editori), **Diversitatea lingvistică și dialogul intercultural în procesul de comunicare**,  2 vols., Garomont Studio, Chișinău / Vasiliana '98, Iași, 2013.\n\n#Articole\n* ***A tradução galego-portuguesa do romance arturiano nos séculos XIII e XIV***, **Literarura artúrica y definiciones del poder en la edad media peninsular**, Heusch, Carlos; Lucía Megías, José Manuel; Miranda, José Carlos Ribeiro (dir.),  E-Spania – Revue interdisciplinaire d’études hispaniques médiévales et modernes, nr. 16 / décembre 2013. \n\n* ***Testemunhos arturianos galego-portugueses***, **Language and Literature-European Landmarks of Identity**, nr. 6, 2010, pp. 134-140.\n","didactic":"* **Lingvistică generală**, anul I, studii de licență\n\n* **Limba română: Sintaxă**, anul al II-lea, studii de licență\n\n* **Istoria limbii române literare**, anul al III-lea, studii de licență\n\n* **Limba română: Stilistică**, anul al III-lea, studii de licență.\n","administrative":"Membru în comisia de realizare a orarului Facultății de Litere\n","photo":"/images/professors/simona-ailenii.jpg","email":"simona-ailenii@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.862Z","updatedAt":"2015-10-04T13:31:00.080Z","id":"55ec67ddaae1a18a09031a1b"},{"events":[],"projects":[],"journals":[],"department":{"name":"Catedra de Limbă română pentru studenți străini","hidden":false,"createdAt":"2015-09-06T16:20:44.230Z","updatedAt":"2015-09-06T16:20:45.064Z","id":"55ec67dcaae1a18a090319f4"},"name":"Ludmila Braniște","username":"ludmila-braniste","level":"Conferenţiar universitar","status":"Cadru didactic titular, **Catedra de Limba română pentru studenți străini**","scientific":"# Volume\n* **Scriitori şi cărţi – studii şi articole de teorie, istorie şi critică literară**, Editura Vasiliana ’98, Iaşi, 2006\n\n* **Comunicare fără frontiere (de la cuvântul rusesc la dialogul românesc)**, „Camera Naţională a Cărţii”, Chişinău, 2001\n\n* **Inadaptabilul – o ipostază a eroului romantic în romanul românesc din secolul al XIX-lea**, Editura Vasiliana ’98, Iaşi, 2000\n\n* **Dramaturgia românească de inspiraţie istorică (secolul al XIX-lea) şi modelul romantic francez**, publicată la Editura Cronica, Iaşi, 1999\n\n* **În munţii Moldovei şi sub cerul Africii. Proza lui Vasile Alecsandri**, Editura Asociaţia Culturală „Grai şi suflet”, Chişinău, 1999\n\n#Traduceri\n* **Respiraţia mitocondrială. O nouă metodă originală de cuantificare a modificărilor funcţionale la cordul uman**, traducere din limba rusă, autori: Tudor Branişte & Valdur Saks, Editura’98, Iaşi, 2014\n\n* **Cardiomiopatia dilatativă. Investigaţii analitice clinico-instrumentale, morfologice şi molecular-celulare la bolnavii cu CMD. Diagnostic diferenţial şi prognostic**, traducere din limba rusă, autori, Tudor Branişte & Vladimir Naumov, Editura’98, Iaşi, 2010\n\n \n#Articole (selectiv)\n* ***Mihail Sadoveanu. Modele literare mărturisite***, Academia Română – Filiala Iaşi, Institutul de  Filologie Română „A. Philippide”, în volumul **Al. Andriescu - 88**, coordonator: Gabriela Haja, Iaşi, Editura Universităţii „Alexandru Ioan Cuza”, 2014\n\n* **Un modèle romantique français exponentiel dans la structure des écrits roumains du xixe siècle***, în **Globalization and intercultural dialogue: multidisciplinary perspectives**, editor: Iulian Boldea, Editura Arhipelag XXI, Tîrgu-Mureş, Volume Number 1, Section: Literature, (C) Arhipelag XXI Press, 2014\n\n* ***French Romanticism as an Important Catalyser in Romanian Literature***, in **Globalization and intercultural dialogue: multidisciplinary perspectives**, editor: Iulian Boldea, Tîrgu-Mureş,  Arhipelag XXI Press, Volume Number 1, Section: Literature, © Arhipelag XXI Press, 2014\n\n* ***The complexity of interlanguage - a computer assisted analysis***, (în colaborare), in **The Proceedings of ICVL (International Conference on Virtual Learning)**, ISI Proceedings (Thomson Reuters), 2014\n\n* ***La catégorie de la diathèse aux niveaux sémantique et syntactique de l’analyse des phrases***, în volumul **Constructions identitaires. Réverbérations du modèle culturel français en contexte européen et universel/ Construcţii identitare. Ecouri ale modelului cultural francez în context european şi universal**, vol. 3, **Linguistique et Didactique/ Lingvistică & Didactică**, coordonatori: Gina Nimigean & Ludmila Braniste, editori ştiinţifici: Iolanda Sterpu & Mariana-Diana Câşlaru; coordonator de serie [în 3 vol.]: Ludmila Branişte,Editura Presa universitară bălţeană, Bălţi, Editura Vasiliana’98, Iași, 2014\n\n* ***On Some Specific Features of Using Computer Technologies in Foreign Language Teaching*** (în colaborare), in **The Proceedings of ICVL (International Conference on Virtual Learning)**, 2014\n\n* ***Révérbations du modèle culturel français dans l’espace de romantisme européen. La nature dans la poésie de Mihai Eminescu et la méthode statistique lexicale***, în volumul II al revistei **La Francopolyphonie**, Chişinău, ULIM, nr. 9, 2014\n\n* ***Le rôle du texte dans le développement de la compétence communicative dans une langue étrangère***, în **Bulletin scientifique de l’université « Politehnica » de Timișoara, Série Langues Modernes**, 2014\n\n* ***Statutul ştiinţific al limbii române în universităţile străine – catalizator al lingvisticii şi literaturii româneşti/The Scientific Status of Romanian Language in Foreign Universities – Catalyst of Romanian Linguistics and Literature***, în revista **Europa**, Novi Sad, Serbia, nr. 12/2013\n\n* ***Despre bazele didacticii limbii române***, în **Diversitatea lingvistică și dialogul intercultural în procesul de comunicare**, coordonatori: Ludmila Branişte și Gina Nimigean, Editura Universității Pedagogice de Stat „Ion Creangă”, Chișinău, Editura „Vasiliana ’98”, Iași, 2013\n\n* ***On some secific nouns+noun non-prepositional word combinations***, în volumul **Limbă, cultură şi\ncivilizaţie**, coordonator: Yolanda-Mirela Catelly, Bucureşti, Editura Politehnica Press, 2013\n\n* ***Method and Creation in the Objectual Teaching Process in Superior Education and Its Actional Perspective***, in **The Proceedings of the Studies on literature, discourse and multicultural dialogue Congress**, Section: Communication and Public Relations/ Editura Universităţii „Petru Maior”, editor: Iulian Boldea, Târgu Mureş: Arhipelag XXI Press, 2013\n\n* ***Lyrical expresivity in Romanian and the art of translation***, (în colaborare),  in **The Proceedings of the European Integration-between Tradition and Modernity Congress**, Editura Universităţii „Petru Maior”, Volume Number 5,editor: Iulian Boldea, Târgu Mureş: Arhipelag XXI Press, 2013\n\n* ***Communicational approach in teaching Romanian as a foreign language in Romanian higher education system***, in **The Proceedings of the European Integration-between Tradition and Modernity Congress**, Editura Universităţii „Petru Maior”, Volume Number 5, editor: Iulian Boldea, Târgu Mureş: Arhipelag XXI, 2013\n\n* ***Incursions dans l’histoire de l’europenisme roumain: erudition et sentiment dans l’existence et l’ecriture d’un ami devoué des Roumains***, în **La francopolyphonie**, 7, Nr. 2/2012\n\n* ***Modificarea structurilor gramaticale – condiţie obligatorie în folosirea activă a limbii***, în volumul **Noi perspective în abordarea românei ca limbă străină/ca limbă nematernă**, editori Elena Platon, Antonela Arieşan, „Casa Cărţii de Stiinţă”, Cluj-Napoca, 2012\n\n* ***Ana Guţu: un stil de a trăi – un mod de a scrie***, în volumul **La liberté de la création au feminin. In honorem Ana Guţu**, editor Elena Prus, Editura Universităţii Libere Internaţionale din Moldova, Chişinău 2012 \n\n* ***Din „modesta” ei istorie, femeia a atacat „marele” său destin***, în volumul **La liberté de la création au feminin. In honorem Ana Guţu**, editor Elena Prus, Editura Universităţii Libere Internaţionale din Moldova, Chişinău 2012\n\n* ***Une hypostase representative des relations culturelles et littéraires entre la France et la Roumanie***, in **Journal of Humanistic and Social Studies, Faculty of Humanities and Social Sciences of „Aurel Vlaicu” University**, Arad, volume II, No. 2 (4)/ 2011","didactic":"Didactic [text aici...]","administrative":"Șefă a **Catedrei de Limbă română pentru Studenți Străini**","photo":"/images/professors/ludmila-braniste.jpg","email":"ludmila-braniste@romanistica.uaic.ro","admin":false,"hidden":false,"createdAt":"2015-09-06T16:20:45.521Z","updatedAt":"2015-10-04T13:46:41.795Z","id":"55ec67ddaae1a18a09031a09"}];

var updateUsers = function(next) {
	async.series(users.map(function(user) {
		return function(next) {
			User.update({username: user.username}, {
				motto: user.motto,
				status: user.status,
				scientific: user.scientific,
				didactic: user.didactic,
				administrative: user.administrative,
				email: user.email,
			}, next);
		};
	}), next);
};

var updateUsersContact = function(next) {
	async.series(users.map(function(user) {
		return function(next) {
			User.update({username: user.username}, {
				contact: user.contact,
			}, next);
		};
	}), next);
};

var removeFunnyCharacterFromCatalin = function(next) {
	User.update({username: 'catalin-mihai-constantinescu'}, {
		scientific: "**CERCETARE ȘTIINȚIFICĂ**:\n\nProiect de cercetare (director de proiect):  ***Dicționar de literatură comparată***, 2004-2005 (cod CNCSIS 77, no. 33373/2004)\n\nProiect de cercetare ***Reception Theory in the Age of Multimedia***, bursă „Herbert Quandt”-Stiftung la Universität Konstanz, Germany, 2007-2008\n\nMembru al grupului de cercetare ***FAULT LINES OF MODERNITY: NEW CONTEXTS FOR RELIGIONS, ETHICS AND LITERATURE***, la College of Liberal and Creative Arts, San Francisco State University & The ICLA Research Comittee on Religion, Ethics and Literature, 2016-prezent\n\n\n\n**STUDII & ARTICOLE** (selectiv):\n\n*De la Relevanzfigur şi Negation la deconstrucţie tolerată. Modelul Karlheinz Stierle*, în “Philologica Jassyensia”, Iaşi, Editura Alfa, nr. 1, 2008, pp. 97-110\n\n*Perspective comparatiste: poetica comparată*, în “Acta Iassyensia Comparationis”, Iaşi, nr. 6, 2008, pp. 11-13\n\n*Metaforă şi negaţie în* Metamorfoza *de F. Kafka*, în “Acta Iassyensia Comparationis”, Iaşi, nr. 6, 2008, pp. 72-79\n\n*Jocul reprezentărilor în* Castelul *kafkian*, în “Transilvania”, no.12/2008, Sibiu, 2008\n\n*The Validity of Reader-oriented Criticism in Electronic Media*, în “Philologica Jassyensia”, Anul V, Nr. 1 (9), 2009, pp. 95-102\n\n*Anti-Elitist Challenges*, în „Analele Științifice ale Universității Ovidius din Constanța. Seria Filologie, Vol.XXIV, Nr. 2/2013, pp. 29-33\n\n*Shame and Identity in Philip Roth’s* The Human Stain, in „Acta Iassyensia Comparationis”, nr. 13, 1/2014, pp. 49-52 \n\nIntermedialität *and Literature. What is Filmic Rewriting?*, in ‘Philologica Jassyensia’, An XI, Nr.1 (21), 2015, pp. 165-174\n\n*Postmodernist Readings of the Relationship between Space and Identity*, in Metacritic Journal for Comparative Studies and Theory, volume 2, issue 1, July 2016, pp. 73-90\n\nCONFERINȚE ȘI COMUNICĂRI PREZENTATE LA MANIFESTĂRI ȘTIINȚIFICE (selectiv):\n\nRezeptionsästhetik *– Crossroads and Challenges in the Space of Hypertext and Hypermedia*, “Herbert-Quandt-Stiftungs Abschluss Tagung”, Konstanz University, Germany, 24 July 2008\n\n*The Role of Shame in Literature*, The 20th Congress of International Comparative Literature Association (AILC-ICLA), Paris-Sorbonne, 18-24 July 2013 \n\n*Literature as Exploration. Reading the Emotions*, International Colloquim ‘Epoca marilor deschideri: Rolul limbilor şi al literaturilor în societatea pluralistă’, Chişinău, Moldova, 27-28 March 2014\n\n*Spațiu și identitate în* Jacob se hotărăște să iubească *de Cătălin Dorian Florescu*, Simpozionul Internațional „Räume und Medien in der Romania”/„Spații și medii în culturile romanice”, Universität Leipzig, Germany, 27-30 September 2015\n\n*Das Bild des Juden in Erzählungen von Ion Luca Caragiale*, “Räume und Menschen in Geschichte, Literature und Politik Rumäniens (19.-21.Jahrhundert)” Tagung – 3. Studientag Rumänien, Johannes Gutenberg-Universität Mainz, 2 November 2015\n\n*Rolul traducerilor în teoria generală a literaturii*, The 5th International Conference of Romanian Studies „Translations We Live by: Romanian at the Crossroads of Multilingualism”, University of Lisbon, 16-17 May 2016\n\n*Trauma, Emotions and Ethics in the Narratives of* Martin Amis, Research Panel „The Text as Being – Ontologies of Repair, Redemption and Regret”, The 21th Congress of ICLA/AILC, University of Vienna, 21-27 July 2016",	
	}, next);
}

module.exports.bootstrap = function(next) {
	async.series(
		[
			// Old bootstrap scripts (should be commented out!)
			// insertDepartments,
			// insertProjects,
			// addNewUsers,
			// updateUsersPhotos,
			// updateAlexandruCondurachePhoto,
			// updateUsers,
			// updateUsersContact,

			// New bootstrap scripts.
			// deleteDanStoica,
			// deleteDanielCondurache,
			// updateBogdanCretuProfessor,
			// updateIlieMoisucLecturer,
			// updateGinaNimigeanLecturer,
			// updateMirceaPaduraruLecturer,
			// deleteDanielCondurache,
			// deleteAlexandruCondurache,
			// deleteIoanaMoldovanuCenusa,
			// deleteVioletaCincu,
			// deleteVladinaMunteanu,
			// deleteZahariaCeausu,
			// insertCodrutaAntonesei,
			// updateNamesAndEmails,
			// deletePuiuIonita,
			// updateSimonaAileniiName,
			// updateGinaNimigeanEmail,
			removeFunnyCharacterFromCatalin,
		],

		function(err, results) {
			if (err) {
				console.log('Could not add fixtures');
				return console.log(err);
			}

			console.log('Added the fixtures');
			next();
		}
	);
};
