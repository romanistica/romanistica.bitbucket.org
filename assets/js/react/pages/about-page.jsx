var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var AboutPage = React.createClass({
	render: function() {
		return (
			<div className="about-page table">
				<div className="row">
					<div className="col">
						<div className="container">
							<h1>Despre</h1>
							<article>
								<h2>Titlu despre Catedra</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas feugiat tellus, quis condimentum nibh efficitur eget. In arcu diam, mollis sit amet tempor sit amet, dictum a augue. Nulla tempus diam a metus lacinia, vel aliquet odio tristique. In venenatis lobortis ex at egestas. Quisque faucibus euismod turpis eu rutrum. Morbi fringilla arcu vitae metus tempus, id scelerisque nulla dignissim. Fusce vitae lorem suscipit turpis venenatis varius. Nunc semper ipsum bibendum ipsum hendrerit, eu auctor turpis tempor.</p>
								<section>
									<h3>Titlu despre Catedra</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas feugiat tellus, quis condimentum nibh efficitur eget. In arcu diam, mollis sit amet tempor sit amet, dictum a augue. Nulla tempus diam a metus lacinia, vel aliquet odio tristique. In venenatis lobortis ex at egestas. Quisque faucibus euismod turpis eu rutrum. Morbi fringilla arcu vitae metus tempus, id scelerisque nulla dignissim. Fusce vitae lorem suscipit turpis venenatis varius. Nunc semper ipsum bibendum ipsum hendrerit, eu auctor turpis tempor.</p>
								</section>
							</article>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

if (isNode) {
	exports.AboutPage = AboutPage;
}