/**
* Journal.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,

  attributes: {
    name: {
      type: 'string',
    },
    
    hidden: {
      type: 'boolean',
      defaultsTo: false,
    },

    description: {
      type: 'string',
    },
    
    editions: {
      type: 'string',
      required: true,
    },
    
    idb: {
      type: 'string',
    },
    
    photo: {
      type: 'string',
    },
    
    users: {
      collection: 'User',
      via: 'journals',
    },
  }
};

