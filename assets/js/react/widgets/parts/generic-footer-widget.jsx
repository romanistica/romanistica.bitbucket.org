var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var GenericFooterWidget = React.createClass({
	render: function() {
		return (
			<div className="generic-footer-widget" key="generic-footer-widget">
				<p>Copyright &copy; 2018. Departamentul de Românistică, Jurnalism - Științele Comunicării și Literatură Comparată. Toate drepturile rezervate.</p>
			</div>
		);	
	}
});

if (isNode) {
	exports.GenericFooterWidget = GenericFooterWidget;
}