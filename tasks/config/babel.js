/**
 * Babelize files.
 *
 * ---------------------------------------------------------------
 *
 * Babelizes files from a defined folder.
 *
 * For usage docs see:
 * 		https://www.npmjs.com/package/grunt-babel
 */
module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		babel: {
			options: {
				sourceMap: true
			},
			dist: {
				files: [{
					"expand": true,
					"cwd": "assets/js/react",
					"src": ["**/*.jsx"],
					"dest": "assets/js/react",
					"ext": ".js"
				}]
			}
		}
	});
};
