var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

EventWidget = isNode ? require('../widgets/entities/event-widget.jsx').EventWidget : window.EventWidget;

var EventsPage = React.createClass({
	propTypes: {
		events: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
	},
	
	render: function() {
		var self = this;
		
		return (
			<div className="events-page table">
				<div className="row">
					<div className="col">
						<div className="container">
							<h1>Evenimente</h1>
							<div className="events">
							
								<div className="event-widget">
									<h1>Conferința &quot;Limbă, Literatură, Comunicare&quot;</h1>
									<p className="location">Facultatea de Litere, UAIC</p>
									<p className="time">20 - 21 Noiembrie</p>
									<p>
										<a target="_blank" href="/conferinta-limba-literatura-comunicare-2015">spre pagina conferinței</a>
									</p>
								</div>
							
								{self.props.events.map(function(event) {
									return (
										<EventWidget event={event} />
									);
								})}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

if (isNode) {
	exports.EventsPage = EventsPage;
}