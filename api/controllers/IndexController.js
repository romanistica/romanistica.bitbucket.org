/* global Department */
/* global Journal */
/* global Project */
/* global User */
/* global EnumService */
/**
 * IndexController
 *
 * @description :: Server-side logic for managing Indices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
 
require('node-jsx').install();
var async = require('async');
var fs = require('fs');

module.exports = {
  
  /**
   * `IndexController.getLanding()`
   */
  getLanding: function (req, res) {
    async.parallel([
      function(next) {
        if (req.session && req.session.User) {
          return next(null, req.session.User);
        }
        
        return next(null, null);
      },
      
      function(next) {
        User.find({ hidden: false }).populateAll().exec(next);
      },
      
      function(next) {
        Event.find({ hidden: false }).populateAll().exec(next);
      },
      
      function(next) {
        Project.find({ hidden: false }).populateAll().exec(next);
      },
      
      function(next) {
        Journal.find({ hidden: false }).populateAll().exec(next);
      },
      
      function(next) {
        return next(null, EnumService.userLevels);
      },

      function(next) {
        Department.find({ hidden: false }).populateAll().exec(next);
      },
    ], function(err, results) {
      var auth = results[0];
      var users = results[1];
      var events = results[2];
      var projects = results[3];
      var journals = results[4];
      var userLevels = results[5];
      var departments = results[6];
      
      return res.view('index/landing', {
        layout: 'layout',
        auth: auth,
        users: users,
        events: events,
        projects: projects,
        journals: journals,
        userLevels: userLevels,
        departments: departments,
      });
    });
  },
  
  getLLC2015: function(req, res) {
    var target = 'LLC2015/index.html';

    fs.exists(target, function (exists) {
      if (!exists) {
        var errorHtml = new Error('The html was not found.');
        errorHtml.name = 'Error with the user';
        errorHtml.code = 'errorUserNotFound';
        errorHtml.localize = true;
        return res.notFound(errorHtml);
      }

      fs.createReadStream(target).pipe(res);
    });
  },
};