/* global Department */
/* global EnumService */
/* global Journal */
/* global Project */
/* global User */
/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async = require('async');

module.exports = {
	'new': function(req, res) {
		res.view('user/new.ejs');
	},
	
	'show': function(req, res, next) {
		async.parallel([
			function(next) {
				if (req.session && req.session.User) {
					return next(null, req.session.User);
				}
				
				return next(null, null);
			},
			
			function(next) {
				User.findOne().where({
			or: [
				{
					id: req.param('id')
				},
				{
					username: req.param('id')
				}
			]
		}).populateAll().exec(next);
			},
		], function(err, results) {
			var auth = results[0];
			var user = results[1];
		
			return res.view('user/show', {
				layout: 'layout',
		auth: auth,
				user: user,
			});
		});
	},
	
	'edit': function(req, res, next) {
		async.parallel([
			function(next) {
				if (req.session && req.session.User) {
					return next(null, req.session.User);
				}
				
				return next(null, null);
			},
			
			function(next) {
				User.findOne().where({
					or: [
						{
							id: req.param('id')
						},
						{
							username: req.param('id')
						}
					]
				}).populateAll().exec(next);
			},
		
			function(next) {
				Department.find({ hidden: false }).populateAll().exec(next);
			},
		
			function(next) {
				return next(null, EnumService.userLevels);
			},		  
		], function(err, results) {
			var auth = results[0];
			var user = results[1];
			var departments = results[2];
			var userLevels = results[3];
		
			return res.view('user/edit', {
				layout: 'layout',
				auth: auth,
				user: user,
				departments: departments,
				userLevels: userLevels,
			});
		});
	},

	'index': function(req, res, next) {
	    async.parallel([
	      function(next) {
	        User.find({ hidden: false }).populateAll().exec(next);
	      },
	      
	      function(next) {
	        return next(null, EnumService.userLevels);
	      },
	
	      function(next) {
	        Department.find({ hidden: false }).populateAll().exec(next);
	      },
	    ], function(err, results) {
	      var users = results[0];
	      var userLevels = results[1];
	      var departments = results[2];
	      
	      return res.view('user/index', {
	        layout: 'layout',
	        users: users,
	        userLevels: userLevels,
	        departments: departments,
	      });
	    });	
	},
};
