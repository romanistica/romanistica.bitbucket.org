module.exports = {
	userLevels: {
		// Lengths of these two should be kept the same.
		singulars: ['Profesor universitar', 'Conferenţiar universitar', 'Lector universitar', 'Asistent universitar'],
		plurals: ['Profesori universitari', 'Conferenţiari universitari', 'Lectori universitari', 'Asistenţi universitari'],
	},
};