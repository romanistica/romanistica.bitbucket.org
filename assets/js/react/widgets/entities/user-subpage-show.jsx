var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var UserSubpageShow = React.createClass({
	propTypes: {
		user: React.PropTypes.object,
		title: React.PropTypes.string.isRequired,
		text: React.PropTypes.string,
	},
	
	render: function() {
		var self = this;
		var converter = new Markdown.Converter();
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				<div className="user-subpage" key="user-subpage">
					<h2>{self.props.title}</h2>
					<div className="content" dangerouslySetInnerHTML={{__html: converter.makeHtml(self.props.text)}} />
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.UserSubpageShow = UserSubpageShow;
}