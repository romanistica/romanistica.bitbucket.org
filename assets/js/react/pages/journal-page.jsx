var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var JournalPage = React.createClass({
	propTypes: {
		journal: React.PropTypes.object,
	},

	render: function() {
		var self = this;

		if (!self.props.journal) {
			return (<div className="journal-widget" />);
		}
		
		return (
			<div className="user-page">
				<img src="images/profs/alexandru.gafton.jpg" alt="Alexandru Gafton" />
				<h1>Jurnalul Universitaria</h1>
				<h2>Locaţia</h2>
				<h3>Perioada</h3>
				<article>
					<h4>Didactic</h4>
					<section>Lorem Ipsum</section>
				</article>
				<article>
					<h4>Ediţii</h4>
					<nav>
					</nav>
				</article>
				<article>
					<h4>Persoane</h4>
					<nav>
					</nav>
				</article>
			</div>
		);
	}
});

if (isNode) {
	exports.JournalPage = JournalPage;
}