var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var GenericHeaderWidget = React.createClass({
	propTypes: {
		auth: React.PropTypes.object,
	},

	getInitialState: function() {
		return {};
	},
	
	attemptLogin: function(e) {
		var self = this;
		e.preventDefault();
		
		$.get('/csrfToken', function(response) {
			self.refs.csrfLogin.getDOMNode().value = response._csrf;
			self.refs.formLogin.getDOMNode().submit();
		});
	},
	
	attemptLogout: function() {
		var self = this;
		
		$.get('/csrfToken', function(response) {
			self.refs.csrfLogout.getDOMNode().value = response._csrf;
			self.refs.formLogout.getDOMNode().submit();
		});
	},

	render: function() {
		var self = this;
		
		return (
			<div className="generic-header-widget" key="generic-header-widget">
				{self.props.auth &&
					<form ref="formLogout" action="/session/destroy" method="POST">
						<p>
							<a className="auth" href={"/user/show/" + self.props.auth.username}>Conectat ca {self.props.auth.name} {self.props.auth.admin ? ' (admin)' : ''}</a>
							<a className="btn" onClick={self.attemptLogout}>deconectează</a>
							<input ref="csrfLogout" name="_csrf" type="hidden" />
						</p>
					</form>
				}
				
				{!self.props.auth &&
					<form ref="formLogin" action="/session/create" method="POST" onSubmit={self.attemptLogin}>
						<p>
							<input type="text" placeholder="email" name="email" />
							<input type="password" placeholder="parolă" name="password" />
							<button className="btn" type="submit">conectează</button>
							<input ref="csrfLogin" name="_csrf" type="hidden" />
						</p>
					</form>
				}
			</div>
		);
	}
});

if (isNode) {
	exports.GenericHeaderWidget = GenericHeaderWidget;
}