#!/bin/bash

sudo rm -r .tmp
sudo find ./assets/js/react -name "*.js" -type f -delete
sudo find ./assets/js/react -name "*.js.map" -type f -delete