var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var JournalWidget = React.createClass({
	propTypes: {
		journal: React.PropTypes.object.isRequired,
	},

	render: function() {
		var self = this;
		
		return (
			<div className="journal-widget">
				<img src="images/profs/alexandru.gafton.jpg" alt="Alexandru Gafton" />
				<h1>Jurnalul Universitaria</h1>
				<h2>Locaţia</h2>
				<h3>Perioada</h3>
			</div>
		);
	}
});

if (isNode) {
	exports.JournalWidget = JournalWidget;
}