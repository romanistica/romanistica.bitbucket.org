/* global User */
/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcryptjs');

module.exports = {
	'new': function(req, res) {
		res.view('session/new');
	},
	
	'create': function(req, res, next) {
		if (!req.param('email') || !req.param('password')) {
			var usernamePasswordRequiredError = [{
				name: 'usernamePasswordRequired',
				message: 'You must enter both a username and password.'
			}];
			
			req.session.flash = {
				err: usernamePasswordRequiredError
			};
			
			return res.redirect('/');
		}
		
		User.findOne().where({
			or: [
				{
					email: req.param('email')
				},
				{
					username: req.param('email')
				}
			]
		}).populateAll().exec(function(err, user) {
			if (err) {
				return next(err);
			}
			
			if (!user) {
				var noAccountError = [{
					name: 'noAccount',
					message: 'The email address ' + req.param('email') + ' was not found.'
				}];
				
				req.session.flash = {
					err: noAccountError
				};
				
				return res.redirect('/');
			}
			
			bcrypt.compare(req.param('password'), user.encryptedPassword, function(err, valid) {
				if (err) {
					return next(err);
				}
				
				if (!valid) {
					var usernamePasswordMismatchError = [{
						name: 'usernamePasswordMismatch',
						message: 'The username and password do not match.'
					}];
					
					req.session.flash = {
						err: usernamePasswordMismatchError
					};
					
					return res.redirect('/');
				}
				
				req.session.authenticated = true;
				req.session.User = user;

				user.save(function(err, user) {
					if (err) {
						return next(err);
					}
					
					if (!user) {
						var noAccountError = [{
							name: 'noAccount',
							message: 'The user could not be found.'
						}];
						
						req.session.flash = {
							err: noAccountError
						};
						
						return res.redirect('/');
					}
					
					res.redirect('/user/show/' + user.id);
				});
			});
		});
	},
	
	'destroy': function(req, res, next) {
		if (!req.session.User) {
			return next();
		}
		
		User.findOne(req.session.User.id, function(err, user) {
			if (err) {
				return next(err);
			}
			
			if (!user) {
				var noAccountError = [{
					name: 'noAccount',
					message: 'The user was not found.'
				}];
				
				req.session.flash = {
					err: noAccountError
				};
				
				return res.redirect('/');
			}
			
			req.session.destroy();
			res.redirect('/');
		});
	}
};
