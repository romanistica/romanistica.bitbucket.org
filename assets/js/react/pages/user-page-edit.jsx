var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

GenericHeaderWidget = isNode ? require('../widgets/parts/generic-header-widget.jsx').GenericHeaderWidget : window.GenericHeaderWidget;
UserSubpageEdit = isNode ? require('../widgets/entities/user-subpage-edit.jsx').UserSubpageEdit : window.UserSubpageEdit;

var UserPageEdit = React.createClass({
	propTypes: {
		auth: React.PropTypes.object.isRequired,
		user: React.PropTypes.object.isRequired,
		userLevels: React.PropTypes.object.isRequired,
	},
	
	getInitialState: function() {
		return {
			loading: false
		};
	},
	
	setSubpage: function(subpage) {
		var self = this;
		
		return function() {
			self.setState(React.addons.update(self.state, {
				subpage: {$set: subpage},
			}));
		};
	},

	moveUp: function() {
		var self = this;
		
		$('html, body').animate({
			scrollTop: $('.users-page').offset().top
		});
	},
	
	update: function(data, next) {
		var self = this;
		
		$.get('/csrfToken', function(response) {
			data._csrf = response._csrf;
			
			$.ajax({
				method: 'PUT',
				url: '/user/' + self.props.user.id,
				data: data,
			}).done(next);
		});
	},
	
	updateTargetValue: function(next) {
		return function(event) {
			var value = event.target.value;
			next(value);
		};
	},
	
	updateName: function(name) {
		var self = this;
		self.update({ name: name }, function() {
			self.setState(React.addons.update(self.state, {
				updatedName: {$set: name},
				hasUpdatedName: {$set: true},
			}));		
		});
	},

	updatePassword: function() {
		var self = this;

		if (self.state.loading) {
			return;
		}
		
		self.setState(React.addons.update(self.state, {
			loading: {$set: true},
		}), function() {
			let originalPassword = self.state.updatedOriginalPassword;
			let confirmationPassword = self.state.updatedConfirmationPassword;
	
			if (!originalPassword || !confirmationPassword || originalPassword !== confirmationPassword) {
				self.setState(React.addons.update(self.state, {
					loading: {$set: false},
				}));
			}
	
			self.update({ updatedPassword: originalPassword }, function() {
				self.setState(React.addons.update(self.state, {
					loading: {$set: false},
				}));
			});
		});
	},

	updateOriginalPassword: function(password) {
		var self = this;
		self.setState(React.addons.update(self.state, {
			updatedOriginalPassword: {$set: password},
		}));
	},

	updateConfirmationPassword: function(password) {
		var self = this;
		self.setState(React.addons.update(self.state, {
			updatedConfirmationPassword: {$set: password},
		}));
	},
	
	updateLevel: function(level) {
		var self = this;
		self.update({ level: level.length ? level : null }, function() {
			self.setState(React.addons.update(self.state, {
				updatedLevel: {$set: level},
				hasUpdatedLevel: {$set: true},
			}));		
		});
	},
	
	updateDepartment: function(department) {
		var self = this;
		self.update({ department: department ? department : null }, function() {
			self.setState(React.addons.update(self.state, {
				updatedDepartment: {$set: department},
				hasUpdatedDepartment: {$set: true},
			}));		
		});
	},
	
	updateStatus: function(status, next) {
		var self = this;
		self.update({ status: status }, function() {
			self.setState(React.addons.update(self.state, {
				updatedStatus: {$set: status},
				hasUpdatedStatus: {$set: true},
			}), next);
		});
	},
	
	updateScientific: function(scientific, next) {
		var self = this;
		self.update({ scientific: scientific }, function() {
			self.setState(React.addons.update(self.state, {
				updatedScientific: {$set: scientific},
				hasUpdatedScientific: {$set: true},
			}), next);
		});
	},
	
	updateDidactic: function(didactic, next) {
		var self = this;
		self.update({ didactic: didactic }, function() {
			self.setState(React.addons.update(self.state, {
				updatedDidactic: {$set: didactic},
				hasUpdatedDidactic: {$set: true},
			}), next);
		});
	},
	
	updateAdministrative: function(administrative, next) {
		var self = this;
		self.update({ administrative: administrative }, function() {
			self.setState(React.addons.update(self.state, {
				updatedAdministrative: {$set: administrative},
				hasUpdatedAdministrative: {$set: true},
			}), next);
		});
	},
	
	updateContact: function(contact, next) {
		var self = this;
		self.update({ contact: contact }, function() {
			self.setState(React.addons.update(self.state, {
				updatedContact: {$set: contact},
				hasUpdatedContact: {$set: true},
			}), next);
		});
	},	

	moveUp: function() {
		var self = this;
		
		$('html, body').animate({
			scrollTop: $('.users-page').offset().top
		});
	},	

	render: function() {
		var self = this;
		
		if (!self.props.user) {
			return (<div className="user-page" />);
		}
		
		var topDepartment = self.state.hasUpdatedDepartment ? self.state.updatedDepartment : (self.props.user.department ? self.props.user.department.id : '');
		var topLevel = self.state.hasUpdatedLevel ? self.state.updatedLevel : self.props.user.level;
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				<GenericHeaderWidget auth={self.props.auth} />
				<div className="user-page" key="user-page">
					{self.props.isEmbedded &&
						<a onClick={self.moveUp}>
							<i className="pe-7s-angle-up" ></i>
						</a>
					}
					{!self.props.isEmbedded &&
						<a href={"/user/show/" + self.props.user.id}>
							<i className="pe-7s-angle-up"></i>
						</a>
					}
					<div className="container">
						{self.props.user.photo &&
							<img src={self.props.user.photo} className="avatar"
								alt={self.props.user.name} title={self.props.user.name} />
						}
						<hgroup>
							<input type="text" className="edit-h1" defaultValue={self.state.hasUpdatedName ? self.state.updatedName : self.props.user.name} onChange={self.updateTargetValue(self.updateName)} />
						</hgroup>
						<hgroup>
							<div className="table filters">
								<div className="row">
									<div className="col filters">
										<h2>Catedra</h2>
										<p>
											<input type="radio" id="department-all-checkbox" name="department" value="" onChange={self.updateTargetValue(self.updateDepartment)} checked={!topDepartment || topDepartment.length <= 0} />
											<label htmlFor="department-all-checkbox"><span className="radio"></span> Nicio catedră</label>
										</p>
										{self.props.departments.map(function(department) {
											return (
												<p key={"department-" + department.id}>
													<input type="radio" id={"department-checkbox-" + department.id} name="department" value={department.id} onChange={self.updateTargetValue(self.updateDepartment)} checked={topDepartment === department.id} />
													<label htmlFor={"department-checkbox-" + department.id}><span className="radio"></span> {department.name.replace('Catedra de', '')}</label>
												</p>
											);
										})}
									</div>
									<div className="col filters">
										<h2>Titlul academic</h2>
										<p>
											<input type="radio" id="level-all-checkbox" name="level" value="" onChange={self.updateTargetValue(self.updateLevel)} checked={!topLevel || topLevel.length <= 0} />
											<label htmlFor="level-all-checkbox"><span className="radio"></span> Niciun titlu</label>
										</p>
										{self.props.userLevels.singulars.map(function(level, index) {
											return (
												<p key={"title-" + self.props.userLevels.plurals[index]}>
													<input type="radio" id={"level-checkbox-" + index} name="level" value={self.props.userLevels.singulars[index]} onChange={self.updateTargetValue(self.updateLevel)} checked={topLevel === self.props.userLevels.singulars[index]} />
													<label htmlFor={"level-checkbox-" + index}><span className="radio"></span> {self.props.userLevels.singulars[index]}</label>
												</p>
											);
										})}
									</div>
								</div>
							</div>
						</hgroup>
						<p className="update">
							<span>Parolă nouă</span> <input type="password" className="edit-p" onChange={self.updateTargetValue(self.updateOriginalPassword)} /> <span>Confirmare parolă</span> <input type="password" className="edit-p" onChange={self.updateTargetValue(self.updateConfirmationPassword)} />, <a onClick={self.updatePassword} className={self.state.loading ? 'loading' : ''}>{self.state.loading ? 'se salvează...' : 'schimbați parola'}</a>
						</p>
						<nav className="user-nav">
							<div className={"navitem " + (self.state.subpage === 'status' ? 'selected' : '')} onClick={self.setSubpage('status')}>
								<i className="nav pe-7s-study"></i>
								<br />Afilieri<br />academice
							</div>
							<div className={"navitem " + (self.state.subpage === 'scientific' ? 'selected' : '')} onClick={self.setSubpage('scientific')}>
								<i className="nav pe-7s-pen"></i>
								<br />Activitate<br />ştiinţifică
							</div>
							<div className={"navitem " + (self.state.subpage === 'didactic' ? 'selected' : '')} onClick={self.setSubpage('didactic')}>
								<i className="nav pe-7s-leaf"></i>
								<br />Activitate<br />didactică
							</div>
							<div className={"navitem " + (self.state.subpage === 'administrative' ? 'selected' : '')} onClick={self.setSubpage('administrative')}>
								<i className="nav pe-7s-note"></i>
								<br />Activitate<br />administrativă
							</div>
							<div className={"navitem " + (self.state.subpage === 'contact' ? 'selected' : '')} onClick={self.setSubpage('contact')}>
								<i className="nav pe-7s-mail"></i>
								<br />Modalităţi<br />de contact
							</div>
						</nav>
						{self.state.subpage === 'status' &&
							<UserSubpageEdit title="Afilieri academice" text={self.state.hasUpdatedStatus ? self.state.updatedStatus : self.props.user.status} update={self.updateStatus} />
						}
						{self.state.subpage === 'scientific' &&
							<UserSubpageEdit title="Activitate ştiinţifică" text={self.state.hasUpdatedScientific ? self.state.updatedScientific : self.props.user.scientific} update={self.updateScientific} />
						}
						{self.state.subpage === 'didactic' &&
							<UserSubpageEdit title="Activitate didactică" text={self.state.hasUpdatedDidactic ? self.state.updatedDidactic : self.props.user.didactic} update={self.updateDidactic} />
						}
						{self.state.subpage === 'administrative' &&
							<UserSubpageEdit title="Activitate administrativă" text={self.state.hasUpdatedAdministrative ? self.state.updatedAdministrative : self.props.user.administrative} update={self.updateAdministrative} />
						}
						{self.state.subpage === 'contact' &&
							<UserSubpageEdit title="Modalităţi de contact" text={self.state.hasUpdatedContact ? self.state.updatedContact : self.props.user.contact} update={self.updateContact} />
						}
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.UserPageEdit = UserPageEdit;
}