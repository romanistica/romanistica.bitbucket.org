var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

JournalWidget = isNode ? require('../widgets/entities/journal-widget.jsx').JournalWidget : window.JournalWidget;

var JournalsPage = React.createClass({
	propTypes: {
		journals: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
	},

	render: function() {
		var self = this;
	
		return (
			<div className="journals-page table">
				<div className="row">
					<div className="col">
						<div className="container">
							<h1>Reviste</h1>
							<div className="journals">
								{self.props.journals.map(function(journal) {
									return (
										<JournalWidget journal={journal} />
									);
								})}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

if (isNode) {
	exports.JournalsPage = JournalsPage;
}