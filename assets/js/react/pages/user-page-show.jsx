var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

GenericHeaderWidget = isNode ? require('../widgets/parts/generic-header-widget.jsx').GenericHeaderWidget : window.GenericHeaderWidget;
UserSubpageShow = isNode ? require('../widgets/entities/user-subpage-show.jsx').UserSubpageShow : window.UserSubpageShow;

var UserPageShow = React.createClass({
	propTypes: {
		auth: React.PropTypes.object,
		user: React.PropTypes.object,
		isEmbedded: React.PropTypes.bool,
	},
	
	getInitialState: function() {
		return {
			// Nothing.
		};
	},
	
	setSubpage: function(subpage) {
		var self = this;
		
		return function() {
			if (self.props.isEmbedded) {
				$('html, body').animate({
					scrollTop: $('.user-page').offset().top
				});			
			}
		
			self.setState(React.addons.update(self.state, {
				subpage: {$set: subpage},
			}));
		};
	},
	
	moveUp: function() {
		var self = this;
		
		$('html, body').animate({
			scrollTop: $('.users-page').offset().top
		});
	},	

	render: function() {
		var self = this;
		
		if (!self.props.user) {
			return (<div className="user-page" />);
		}
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				{!self.props.isEmbedded &&
					<GenericHeaderWidget auth={self.props.auth} />
				}
				<div className="user-page" key="user-page">
					{self.props.isEmbedded &&
						<a onClick={self.moveUp}>
							<i className="pe-7s-angle-up" ></i>
						</a>
					}
					{!self.props.isEmbedded &&
						<a href='/user'>
							<i className="pe-7s-angle-up"></i>
						</a>
					}
					<div className="container">
						{self.props.user.photo &&
							<img src={self.props.user.photo} className="avatar"
								alt={self.props.user.name} title={self.props.user.name} />
						}
						<hgroup>
							<h1>{self.props.user.name}</h1>
							{self.props.user.level &&
								<p className="level">{self.props.user.level}</p>
							}
							{self.props.user.department &&
								<p className="department"><a href={'/department/show/' + self.props.user.department.id}>{self.props.user.department.name}</a></p>
							}
							{self.props.isEmbedded &&
								<p className="access"><a href={"/user/show/" + self.props.user.username}>vedeţi pagina personală</a></p>
							}
							{self.props.auth && (self.props.auth.admin || self.props.auth.id === self.props.user.id) &&
								<p className="access"><a href={"/user/edit/" + self.props.user.username}>modificaţi pagină personală</a></p>
							}
						</hgroup>
						{!self.props.isEmbedded &&
							<nav className="user-nav">
								{self.props.user.status &&
									<div className={"navitem " + (self.state.subpage === 'status' ? 'selected' : '')} onClick={self.setSubpage('status')}>
										<i className="nav pe-7s-study"></i>
										<br />Afilieri<br />academice
									</div>
								}
								{self.props.user.scientific &&
									<div className={"navitem " + (self.state.subpage === 'scientific' ? 'selected' : '')} onClick={self.setSubpage('scientific')}>
										<i className="nav pe-7s-pen"></i>
										<br />Activitate<br />ştiinţifică
									</div>
								}
								{self.props.user.didactic &&
									<div className={"navitem " + (self.state.subpage === 'didactic' ? 'selected' : '')} onClick={self.setSubpage('didactic')}>
										<i className="nav pe-7s-leaf"></i>
										<br />Activitate<br />didactică
									</div>
								}
								{self.props.user.administrative &&
									<div className={"navitem " + (self.state.subpage === 'administrative' ? 'selected' : '')} onClick={self.setSubpage('administrative')}>
										<i className="nav pe-7s-note"></i>
										<br />Activitate<br />administrativă
									</div>
								}
								{self.props.user.contact &&
									<div className={"navitem " + (self.state.subpage === 'contact' ? 'selected' : '')} onClick={self.setSubpage('contact')}>
										<i className="nav pe-7s-mail"></i>
										<br />Modalităţi<br />de contact
									</div>
								}
							</nav>
						}
						{!self.props.isEmbedded && self.props.user.status && self.state.subpage === 'status' &&
							<UserSubpageShow title="Afilieri academice" text={self.props.user.status} />
						}
						{!self.props.isEmbedded && self.props.user.scientific && self.state.subpage === 'scientific' &&
							<UserSubpageShow title="Activitate ştiinţifică" text={self.props.user.scientific} />
						}
						{!self.props.isEmbedded && self.props.user.didactic && self.state.subpage === 'didactic' &&
							<UserSubpageShow title="Activitate didactică" text={self.props.user.didactic} />
						}
						{!self.props.isEmbedded && self.props.user.administrative && self.state.subpage === 'administrative' &&
							<UserSubpageShow title="Activitate administrativă" text={self.props.user.administrative} />
						}
						{!self.props.isEmbedded && self.props.user.email && self.state.subpage === 'contact' &&
							<UserSubpageShow title="Modalităţi de contact" text={self.props.user.contact} />
						}
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.UserPageShow = UserPageShow;
}