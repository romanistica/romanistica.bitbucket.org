var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var UserSubpageEdit = React.createClass({
	propTypes: {
		user: React.PropTypes.object,
		title: React.PropTypes.string.isRequired,
		text: React.PropTypes.string,
		update: React.PropTypes.func.isRequired,
	},
	
	getInitialState: function() {
		return {
			converter: null,
			editor: null,
			loading: false,
		};
	},
	
	update: function() {
		var self = this;
		
		if (self.state.loading) {
			return;
		}
		
		self.setState(React.addons.update(self.state, {
			loading: {$set: true},
		}), function() {
			self.props.update(self.refs.textarea.getDOMNode().value, function() {
				self.setState(React.addons.update(self.state, {
					loading: {$set: false},
				}));
			});
		});
	},
	
	componentDidMount: function() {
		var self = this;
		
		var converter = Markdown.getSanitizingConverter();
		var editor = new Markdown.Editor(converter);
		editor.run();
		
		self.setState(React.addons.update(self.state, {
			converter: {$set: converter},
			editor: {$set: editor},
		}));
	},
	
	render: function() {
		var self = this;		
		var converter = new Markdown.Converter();

		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				<div className="user-subpage" key="user-subpage">
					<h2>{self.props.title}</h2>
					<div className="table">
						<div className="row">
							<div className="col">
								<h3>Modificare</h3>
								<div id="wmd-button-bar" />
							</div>
							<div className="col">
								<h3>Previzualizare</h3>
							</div>
						</div>
						<div className="row">
							<div className="col editor">
								<textarea id="wmd-input" ref="textarea" className="edit-content" defaultValue={self.props.text} />
								<p className="update">
									<a onClick={self.update} className={self.state.loading ? 'loading' : ''}>{self.state.loading ? 'se salvează...' : 'salvaţi conţinutul'}</a>
								</p>
							</div>
							<div className="col editor">
								<div id="wmd-preview" className="edit-preview" />
							</div>
						</div>
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.UserSubpageEdit = UserSubpageEdit;
}