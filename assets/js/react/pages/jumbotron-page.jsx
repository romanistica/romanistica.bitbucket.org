var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
$ = isNode ? require('jquery') : window.$;

var JumbotronPage = React.createClass({
	onClickHome: function() {
		$('html, body').css({
			scrollTop: $('.jumbotron-page').offset().top,
			scrollLeft: $('.jumbotron-page').offset().left,
		});
	},
	
	onClickMembers: function() {
		$('html, body').animate({
			scrollTop: $('.users-page').offset().top,
			scrollLeft: $('.users-page').offset().left,
		});
	},
	
	onClickProjects: function() {
		$('html, body').animate({
			scrollTop: $('.projects-page').offset().top,
			scrollLeft: $('.projects-page').offset().left,
		});	
	},
	
	onClickJournals: function() {
		$('html, body').animate({
			scrollTop: $('.journals-page').offset().top,
			scrollLeft: $('.journals-page').offset().left,
		});
	},
	
	onClickEvents: function() {
		$('html, body').animate({
			scrollTop: $('.events-page').offset().top,
			scrollLeft: $('.events-page').offset().left,
		});
	},
	
	onClickAbout: function() {
		$('html, body').animate({
			scrollTop: $('.about-page').offset().top,
			scrollLeft: $('.about-page').offset().left,
		});
	},

	render: function() {
		var self = this;
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				<div className="jumbotron-page table" key="jumbotron-page">
					<div className="row">
						<div className="col">
							<div className="container">
								<div className="nav">
									<a className="home-link" onClick={self.onClickHome}><div className="caption">acasă</div><div className="icon"><i className="pe-7s-home"></i></div></a>
									<a className="members-link" onClick={self.onClickMembers}><div className="caption">membri</div><div className="icon"><i className="pe-7s-users"></i></div></a>
									<a className="projects-link" onClick={self.onClickProjects}><div className="caption">proiecte</div><div className="icon"><i className="pe-7s-science"></i></div></a>
									<a className="journals-link" onClick={self.onClickJournals}><div className="caption">publicaţii</div><div className="icon"><i className="pe-7s-copy-file"></i></div></a>
									<a className="events-link" onClick={self.onClickEvents}><div className="caption">evenimente</div><div className="icon"><i className="pe-7s-note2"></i></div></a>
								</div>
								<div className="logo">
									<img src="/images/letters.png" alt="Facultatea de Litere" />
								</div>
								<h1><a onClick={self.onClickAbout}>Departamentul de Românistică, Jurnalism - Științele Comunicării și Literatură Comparată</a></h1>
								<h2><a href="http://media.lit.uaic.ro" target="_blank">Facultatea de Litere</a></h2>
								<h3><a href="http://www.uaic.ro" target="_blank">Universitatea Alexandru Ioan Cuza din Iaşi</a></h3>
							</div>
						</div>
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.JumbotronPage = JumbotronPage;
}