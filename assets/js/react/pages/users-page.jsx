var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
$ = isNode ? require('jquery') : window.$;

GenericHeaderWidget = isNode ? require('../widgets/parts/generic-header-widget.jsx').GenericHeaderWidget : window.GenericHeaderWidget;
UserWidget = isNode ? require('../widgets/entities/user-widget.jsx').UserWidget : window.UserWidget;

var UsersPage = React.createClass({
	propTypes: {
		users: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		departments: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		userLevels: React.PropTypes.object.isRequired,
		isEmbedded: React.PropTypes.bool,
		setUser: React.PropTypes.func,
	},
	
	getInitialState: function() {
		return {
			department: '',
			level: '',
			query: '',
		};
	},
	
	updateDepartment: function(event) {
		var self = this;
		var department = event.target.value;
		
		self.setState(React.addons.update(self.state, {
			department: {$set: department},
		}), function() {
			self.forceUpdate();
		});
	},
	
	updateQuery: function(event) {
		var self = this;
		var query = event.target.value;
		
		self.setState(React.addons.update(self.state, {
			query: {$set: query},
		}), function() {
			self.forceUpdate();
		});
	},

	updateLevel: function(event) {
		var self = this;
		var level = event.target.value;

		self.setState(React.addons.update(self.state, {
			level: {$set: level},
		}), function() {
			self.forceUpdate();
		});
	},
	
	showFilters: function() {
		var self = this;
		
		self.setState(React.addons.update(self.state, {
			filtersShown: {$set: true},
		}));
	},
	
	moveUp: function() {
		var self = this;
		
		$('html, body').animate({
			scrollTop: $('.jumbotron-page').offset().top
		});
	},	

	deDiacriticize: function(word) {
		// ăâîşșţțĂÂÎŞȘŢȚ
		
		var deDiacriticized = word.toString();
		
		deDiacriticized = deDiacriticized.replace('ă', 'a');
		deDiacriticized = deDiacriticized.replace('â', 'a');
		deDiacriticized = deDiacriticized.replace('î', 'i');
		deDiacriticized = deDiacriticized.replace('ş', 's');
		deDiacriticized = deDiacriticized.replace('ș', 's');
		deDiacriticized = deDiacriticized.replace('ţ', 't');
		deDiacriticized = deDiacriticized.replace('ț', 't');
		
		deDiacriticized = deDiacriticized.replace('Ă', 'A');
		deDiacriticized = deDiacriticized.replace('Â', 'A');
		deDiacriticized = deDiacriticized.replace('Î', 'I');
		deDiacriticized = deDiacriticized.replace('Ş', 'S');
		deDiacriticized = deDiacriticized.replace('Ș', 'S');
		deDiacriticized = deDiacriticized.replace('Ţ', 'T');
		deDiacriticized = deDiacriticized.replace('Ț', 'T');
		
		return deDiacriticized;
	},

	render: function() {
		var self = this;
		
		var filteredUsers = self.props.users.filter(function(user) {

			// Filter by department
			if (!self.state.department || !self.state.department.length) {
				return true;
			}
			
			return user.department.name == self.state.department;
		}).filter(function(user) {

			// Filter by level
			if (!self.state.level || !self.state.level.length) {
				return true;
			}
			
			return user.level === self.state.level;
		}).filter(function(user) {
		
			// Filter by query
			if (!self.state.query || !self.state.query.length) {
				return true;
			}

			return self.deDiacriticize(user.name).toLowerCase().indexOf(self.deDiacriticize(self.state.query).toLowerCase()) >= 0;
		}).map(function(user) {
			return user.id;
		});
		
		var shownUsers = self.props.users.sort(function(user1, user2) {
			var level1 = self.props.userLevels.singulars.indexOf(user1.level);
			var level2 = self.props.userLevels.singulars.indexOf(user2.level);
		
			if (level1 != level2) {
				return level1 - level2;
			}
		
			return user1.name.localeCompare(user2.name);
		});
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				{!self.props.isEmbedded &&
					<GenericHeaderWidget auth={self.props.auth} />
				}
				<div className="users-page" key="users-page">
					<div className="container">
						{self.props.isEmbedded &&
							<a onClick={self.moveUp}>
								<i className="pe-7s-angle-up" ></i>
							</a>
						}
						{!self.props.isEmbedded &&
							<a href='/'>
								<i className="pe-7s-angle-up"></i>
							</a>
						}
						<h1>Membri</h1>
					</div>
					{!self.state.filtersShown &&
						<div className="filters background-white">
							<p className="show-filters">
								<a onClick={self.showFilters}>căutaţi şi filtraţi cadrele didactice după diverse criterii</a>
							</p>
						</div>
					}
					{self.state.filtersShown &&
						<div className="table filters">
							<div className="row">
								<div className="col">
									<h2>Căutare</h2>
									<input type="text" onChange={self.updateQuery} placeholder="scrieţi aici" />
								</div>
								<div className="col">
									<h2>Catedră</h2>
									<p>
										<input type="radio" id="department-all-checkbox" name="department" value="" onChange={self.updateDepartment} checked={self.state.department <= 0} />
										<label htmlFor="department-all-checkbox"><span className="radio"></span> Toate catedrele</label>
									</p>
									{self.props.departments.map(function(department) {
										return (
											<p key={"department-" + department.id}>
												<input type="radio" id={"department-checkbox-" + department.id} name="department" value={department.name} onClick={self.updateDepartment} />
												<label htmlFor={"department-checkbox-" + department.id}><span className="radio"></span> {department.name.replace('Catedra de', '')}</label>
											</p>
										);
									})}
								</div>
								<div className="col">
									<h2>Titlu academic</h2>
									<p>
										<input type="radio" id="level-all-checkbox" name="level" value="" onChange={self.updateLevel} checked={self.state.level.length <= 0} />
										<label htmlFor="level-all-checkbox"><span className="radio"></span> Toate titlurile</label>
									</p>
									{self.props.userLevels.plurals.map(function(level, index) {
										return (
											<p key={"title-" + self.props.userLevels.plurals[index]}>
												<input type="radio" id={"level-checkbox-" + index} name="level" value={self.props.userLevels.singulars[index]} onChange={self.updateLevel} />
												<label htmlFor={"level-checkbox-" + index}><span className="radio"></span> {self.props.userLevels.plurals[index]}</label>
											</p>
										);
									})}
								</div>
							</div>
						</div>
					}
					<div className="users">
						<div className="table">
							<div className="row background-white">
								{self.props.userLevels.singulars.map(function(level, index) {
									return (
										<div className="col levels" key={"user-level-" + index}>
											<div className="levels-level" dangerouslySetInnerHTML={{__html: self.props.userLevels.plurals[index].replace(' ', '<br />')}} />
										</div>
									);
								})}
							</div>
							<div className="row">
								{self.props.userLevels.singulars.map(function(level, index) {
									return (
										<div className="col levels" key={"users-on-level-" + index}>
											{shownUsers.map(function(user) {
												if (user.level !== level) {
													return (<div />);
												}
												
												var isHighlighted =
													filteredUsers.length == shownUsers.length ||
													filteredUsers.indexOf(user.id) >= 0;
											
												return (
													<UserWidget user={user} key={"user-widget-" + user.id} isHighlighted={isHighlighted} setUser={self.props.setUser} />
												);				
											})}
										</div>
									);
								})}
							</div>
						</div>
					</div>
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.UsersPage = UsersPage;
}