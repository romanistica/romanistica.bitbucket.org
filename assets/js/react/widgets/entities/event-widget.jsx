var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var EventWidget = React.createClass({
	propTypes: {
		event: React.PropTypes.object.isRequired,
	},

	render: function() {
		var self = this;
		
		return (
			<div className="event-widget">
				<img src="images/profs/alexandru.gafton.jpg" alt="Alexandru Gafton" />
				<h1>Evenimentul Universitaria</h1>
				<p class="location">Locaţia</p>
				<p class="period">Perioada</p>
			</div>
		);
	}
});

if (isNode) {
	exports.EventWidget = EventWidget;
}
