var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

GenericHeaderWidget = isNode ? require('./widgets/parts/generic-header-widget.jsx').GenericHeaderWidget : window.GenericHeaderWidget;
GenericFooterWidget = isNode ? require('./widgets/parts/generic-footer-widget.jsx').GenericFooterWidget : window.GenericFooterWidget;

JumbotronPage = isNode ? require('./pages/jumbotron-page.jsx').JumbotronPage : window.JumbotronPage;
AboutPage = isNode ? require('./pages/about-page.jsx').AboutPage : window.AboutPage;
EventsPage = isNode ? require('./pages/events-page.jsx').EventsPage : window.EventsPage;
JournalsPage = isNode ? require('./pages/journals-page.jsx').JournalsPage : window.JournalsPage;
ProjectsPage = isNode ? require('./pages/projects-page.jsx').ProjectsPage : window.ProjectsPage;
UsersPage = isNode ? require('./pages/users-page.jsx').UsersPage : window.UsersPage;

EventPage = isNode ? require('./pages/event-page.jsx').EventPage : window.EventPage;
JournalPage = isNode ? require('./pages/journal-page.jsx').JournalPage : window.JournalPage;
ProjectPage = isNode ? require('./pages/project-page.jsx').ProjectPage : window.ProjectPage;
UserPageShow = isNode ? require('./pages/user-page-show.jsx').UserPageShow : window.UserPageShow;

var LandingPage = React.createClass({
	propTypes: {
		auth: React.PropTypes.object,
		users: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		events: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		projects: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		journals: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		departments: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
		userLevels: React.PropTypes.object.isRequired,
	},
	
	getInitialState: function() {
		return {
			// Nothing here.
		};
	},
	
	onClickSignup: function(event) {
		var self = this;
		
		$('html, body').animate({
			scrollTop: $(React.findDOMNode(self.refs.loop)).offset().top
		});
	},
	
	setUser: function(user) {
		var self = this;
		
		return function() {
			self.setState(React.addons.update(self.state, {
				user: {$set: user},
			}), function() {
				$('html, body').animate({
					scrollTop: $('.users').offset().top
				});
			});
		};
	},
	
	render: function() {
		var self = this;
		
		return (
			<ReactCSSTransitionGroup transitionName="react-animate" transitionAppear={true}>
				<div className="landing-page" key="landing-page">
					<GenericHeaderWidget auth={self.props.auth} />
					<JumbotronPage />
					<UsersPage isEmbedded={true} users={self.props.users} userLevels={self.props.userLevels} departments={self.props.departments} setUser={self.setUser} />
					<UserPageShow isEmbedded={true} user={self.state.user} auth={self.props.auth} />
					<EventsPage events={self.props.events} />
					<GenericFooterWidget />
				</div>
			</ReactCSSTransitionGroup>
		);
	}
});

if (isNode) {
	exports.LandingPage = LandingPage;
}

/*
					<AboutPage />

					<EventPage event={self.state.event} />
					<JournalsPage journals={self.props.journals} />
					<JournalPage journal={self.state.journal} />
					<ProjectsPage projects={self.props.projects} />
					<ProjectPage project={self.state.project} />
*/