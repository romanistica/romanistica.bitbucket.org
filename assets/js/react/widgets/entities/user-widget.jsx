var isNode = typeof module !== 'undefined' && module.exports;
React = isNode ? require('react/addons') : window.React;

var UserWidget = React.createClass({
	propTypes: {
		user: React.PropTypes.object.isRequired,
		isHighlighted: React.PropTypes.bool,
		setUser: React.PropTypes.func,
	},
	
	render: function() {
		var self = this;
		
		var content = <img
			className={"user-photo user-widget " + (self.props.isHighlighted ? 'highlighted' : '')}
			src={self.props.user.photo ? self.props.user.photo : '/images/professors/silhouette300x300.png'}
			alt={self.props.user.name}
			title={self.props.user.name}
		/>;
		
		if (self.props.setUser) {
			return (
				<a onClick={self.props.setUser(self.props.user)}>{content}</a>
			);
		}
		
		if (!self.props.setUser) {
			return (
				<a href={'/user/show/' + self.props.user.id}>{content}</a>
			);
		}		
					
		return (<div />);
	}
});

if (isNode) {
	exports.UserWidget = UserWidget;
}