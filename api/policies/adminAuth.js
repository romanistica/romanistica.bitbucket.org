module.exports = function(req, res, ok) {
	if (req.session.User && req.session.User.admin) {
		return ok();
	}
	
	var requireAdminError = [{
    	name: 'requireAdmin',
    	message: 'You must be an admin.'
  }];
  
  req.session.flash = {
    err: requireAdminError
  };
  
  return res.redirect('/');
};