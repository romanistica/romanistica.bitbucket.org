module.exports = function (grunt) {
	grunt.registerTask('compileAssets', [
		'clean:dev',
		'babel:dist',
		'jst:dev',
		'less:dev',
		'copy:dev',
		'coffee:dev'
	]);
};
